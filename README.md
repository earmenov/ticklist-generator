# Ticklist Generator

<img src="uploads/landing-demo.gif" width="700" height="400" >

## Project Description 
Your task is to develop the Playlist Generator web application. Playlist Generator enables your users to generate playlists for specific travel duration periods based on their preferred genres. 
 
A playlist consists of a list of individual tracks(songs). Each track has an artist, title, album, duration (playtime length) and rank (a numeric value). Tracks may or may not have a preview URL to an audio stream (e.g. the users can click and play the preview part of the track). 
Each playlist has a user given title, associated tags (e.g. musical genres), a list of tracks with the associated track details, total playtime (the sum of the playtimes of all tracks in that playlist) and rank (the average of the ranks of all tracks in that playlist). 
 
## Main Usecase 
 
 A user travellling from A to B wants to have something to listen to during the duration of the travel. The user wants to generate a track list based on his musical tastes (the user selects genres from a predefined list). An algorithm, which uses external service as track sample data, generates the playlist. 
 
1)	User MUST be presented with an UI which lets them enter starting and destination address, selects musical genres and clicks “Generate”. 

2)	Playlist Generator calculates the travel duration time between the starting and destination locations and combines tracks chosen randomly in the specified genres, until the playing time roughly matches the travel time duration. Rounding of +/- 5 minutes is allowed (e.g. for a travel duration of 89 minutes a playlist with total playtime between 84 or 94 and is fine). 
 
3)	The generated playlist is saved under this user’s profile and they can start listening to it. 
 
The application offers browsing playlists created by other users and allows filtering by total duration and genre tags. 
 
By default playlists should be sorted by average rank descending. 
 
The application offers certain configuration over the playlist generation algorithm and the possibility to play a preview of the tracks playlist.
 

<img src="uploads/generator-demo.gif" width="700" height="400" >


# Web Application 
 
## Public Part 
The public part of the project should be visible without authentication. This includes the application start page, the user login and user registration forms, as well as the list of all user generated playlists. People that are not authenticated cannot see any user specific details, neither they can interact with the website. They can only browse the playlists and see the tracks list and details of them. 
 
-	Public page should contain at least 3 generated playlists 
-	Playlists can be clicked/expanded to show the list of artists/tracks 
-	Playlists show average rank (avg from tracks ranks) and total playtime 
-	Playlists are sorted by rank and can be filtered by name, genre and duration 
 
## Private Part (Users Only) 
The private part is accessible only to users who have successfully authenticated (registered users). The private part of the web application provides the users the ability to generate new playlists, control the generation algorithm, edit or delete their own existing playlists. 
Editing existing playlists is limited to changing the title or associated genre tags, but does not include editing of the tracklist (e.g. removing or adding individual songs).


<img src="uploads/browse-demo.gif" width="700" height="400" >

 
## Administration Part 
Users with the system administrator role can administer all major information objects. The administrators have the following functionality 
 
-	CRUD over users and other administrators 
-	CRUD over the playlists 
-	Can manually trigger Genre synchronization (if that optional requirement is implemented) 
 

## Database 
The data of the application MUST be stored in a relational database. You need to identify the core domain objects and model their relationships accordingly. 
 
## External Services 
The Playlist Generator web application will consume two public REST services in order to achieve the main functionality. 
 
## Microsoft Bing Maps 
Microsoft Bing Maps offers similar functionality to Google maps but is free for non-commercial use. For usage details please see the Appendix. 
 
## Deezer 
Deezer is a subscription music streaming service similar to Spotify and Google Play. 
The API usage is free for non-commercial use and does not require any registration. For usage details please see the Appendix.  
Technical / Development Requirements 

 
## Backend 
-	The minimum JDK version is 1.8 
-	Use tiered project structure (separate the application components in layers) 
-	For Persistence use MySQL/MariaDB 
-	Use Hibernate/JPA (and/or Spring Data) in the Persistence layer 
-	Use Spring Security to handle user registration and user roles 
-	Service layer (e.g. “business” functionality) should have at least 80% test unit coverage 
 
## Backend Data 
-	Pre-fetch the genres from Deezer and store them in the DB 
-	Choose at least 3 genres and pre-fetch 1,000 tracks from Deezer and store them in the DB 
-	User generated playlists should be stored in the DB 
-	User registration information and credentials should be stored in the DB 
-	Each track should have the following properties stored in the DB – id, title, link, duration, rank, preview URL, artist = {id, name, artist tracklist URL}, album = {id, name, album tracklist URL} 
 
It’s mandatory to create a relational database model between tracks, artists, albums, generated playlists etc. and store them in the database. 
 
Each team is advised to choose at least 3 genres from https://api.deezer.com/genre and pre-fetch at least 1,000 tracks per genre. 
  
 
## Generation Algorithm Spec 
-	MUST NOT repeat tracks 
-	SHOULD NOT repeat artists (unless “allow tracks from the same artists” is selected) 
-	MUST generate random playlists. For example for the same two starting and destination locations, clicking Generate multiple times MUST NOT generate the same playlist twice (repeating tracks/artists between playlists is allowed). 
-	SHOULD allow the user to specify if they want more from specific genre using percentage as shown in the wireframe (or it can be left blank) 
-	Generated playlist total playtime is allowed to be +/- 5mins of the calculated travel duration 
-	Optional “allow tracks from the same artists” - by default the algorithm should not allow the same artist to appear twice in a single generated playlist. This checkbox overrides the default behavior. 
-	Optional “use top tracks” - each track has rank score. If this option is checked the algorithm should pick the highest ranking tracks from the tracks pool in the DB and generate a playlist with them (descending from highest to lowest rank). 
 
## Optional Backend Requirements 
-	implement a synchronization job for the Deezer Genres; The running period should be configurable (e.g. each hour synch the genres from Deezer). In the Admin panel display the last time the synchronization was ran, the status (success/failure) and relevant details (e.g. if failure some detailed message of the Exception) • Implement “use top ranks” functionality 
-	Implement “allow tracks from the same artists” functionality 
-	Implement play functionality using the preview URL of each track. Use browser support or JS to bootstrap a player that can stream audio 
-	Use pixabay REST service to attach images related to music to newly generated playlists, for better presentation on the public page 
-	Use Logger in your application – see Spring Boot 2 logging with logback 
 