-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema playlist_schema
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `playlist_schema` ;

-- -----------------------------------------------------
-- Schema playlist_schema
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `playlist_schema` DEFAULT CHARACTER SET latin1 ;
USE `playlist_schema` ;

-- -----------------------------------------------------
-- Table `playlist_schema`.`album`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `playlist_schema`.`album` ;

CREATE TABLE IF NOT EXISTS `playlist_schema`.`album` (
  `ID` VARCHAR(30) NOT NULL,
  `NAME` VARCHAR(300) NOT NULL,
  `TRACKLIST_URL` VARCHAR(400) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_schema`.`artist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `playlist_schema`.`artist` ;

CREATE TABLE IF NOT EXISTS `playlist_schema`.`artist` (
  `ID` VARCHAR(30) NOT NULL,
  `NAME` VARCHAR(300) NOT NULL,
  `TRACKLIST_URL` VARCHAR(400) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_schema`.`genre`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `playlist_schema`.`genre` ;

CREATE TABLE IF NOT EXISTS `playlist_schema`.`genre` (
  `ID` INT(11) NOT NULL,
  `NAME` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_schema`.`playlist`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `playlist_schema`.`playlist` ;

CREATE TABLE IF NOT EXISTS `playlist_schema`.`playlist` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `TITLE` VARCHAR(60) NOT NULL,
  `COVER_IMAGE` LONGBLOB NOT NULL,
  `TOTAL_DURATION` INT(11) NULL DEFAULT '0',
  `AVG_RANK` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE INDEX `TITLE_UNIQUE` (`TITLE` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 36
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_schema`.`playlist_has_genre`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `playlist_schema`.`playlist_has_genre` ;

CREATE TABLE IF NOT EXISTS `playlist_schema`.`playlist_has_genre` (
  `PLAYLIST_ID` INT(11) NOT NULL,
  `GENRE_ID` INT(11) NOT NULL,
  PRIMARY KEY (`PLAYLIST_ID`, `GENRE_ID`),
  INDEX `fk_playlist_has_genre_genre1_idx` (`GENRE_ID` ASC) VISIBLE,
  INDEX `fk_playlist_has_genre_playlist1_idx` (`PLAYLIST_ID` ASC) VISIBLE,
  CONSTRAINT `fk_playlist_has_genre_genre1`
    FOREIGN KEY (`GENRE_ID`)
    REFERENCES `playlist_schema`.`genre` (`ID`),
  CONSTRAINT `fk_playlist_has_genre_playlist1`
    FOREIGN KEY (`PLAYLIST_ID`)
    REFERENCES `playlist_schema`.`playlist` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_schema`.`track`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `playlist_schema`.`track` ;

CREATE TABLE IF NOT EXISTS `playlist_schema`.`track` (
  `ID` VARCHAR(30) NOT NULL,
  `TITLE` VARCHAR(300) NOT NULL,
  `LINK` VARCHAR(200) NOT NULL,
  `DURATION` VARCHAR(5) NOT NULL,
  `RANK` VARCHAR(11) NOT NULL,
  `PREVIEW_URL` VARCHAR(255) NULL DEFAULT NULL,
  `ALBUM_ID` VARCHAR(30) NOT NULL,
  `ARTIST_ID` VARCHAR(30) NOT NULL,
  `GENRE_ID` INT(11) NOT NULL,
  PRIMARY KEY (`ID`),
  INDEX `fk_track_album` (`ALBUM_ID` ASC) VISIBLE,
  INDEX `fk_track_artist1` (`ARTIST_ID` ASC) VISIBLE,
  INDEX `fk_track_genre1` (`GENRE_ID` ASC) VISIBLE,
  CONSTRAINT `fk_track_album`
    FOREIGN KEY (`ALBUM_ID`)
    REFERENCES `playlist_schema`.`album` (`ID`),
  CONSTRAINT `fk_track_artist1`
    FOREIGN KEY (`ARTIST_ID`)
    REFERENCES `playlist_schema`.`artist` (`ID`),
  CONSTRAINT `fk_track_genre1`
    FOREIGN KEY (`GENRE_ID`)
    REFERENCES `playlist_schema`.`genre` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_schema`.`playlist_has_track`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `playlist_schema`.`playlist_has_track` ;

CREATE TABLE IF NOT EXISTS `playlist_schema`.`playlist_has_track` (
  `PLAYLIST_ID` INT(11) NOT NULL,
  `TRACK_ID` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`PLAYLIST_ID`, `TRACK_ID`),
  INDEX `fk_playlist_has_track_track1_idx` (`TRACK_ID` ASC) VISIBLE,
  INDEX `fk_playlist_has_track_playlist1_idx` (`PLAYLIST_ID` ASC) VISIBLE,
  CONSTRAINT `fk_playlist_has_track_playlist1`
    FOREIGN KEY (`PLAYLIST_ID`)
    REFERENCES `playlist_schema`.`playlist` (`ID`),
  CONSTRAINT `fk_playlist_has_track_track1`
    FOREIGN KEY (`TRACK_ID`)
    REFERENCES `playlist_schema`.`track` (`ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_schema`.`roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `playlist_schema`.`roles` ;

CREATE TABLE IF NOT EXISTS `playlist_schema`.`roles` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uk_roles_name` (`name` ASC) VISIBLE,
  UNIQUE INDEX `UK_nb4h0p6txrmfc0xbrd1kglp9t` (`name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_schema`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `playlist_schema`.`users` ;

CREATE TABLE IF NOT EXISTS `playlist_schema`.`users` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(15) NOT NULL,
  `email` VARCHAR(40) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `createdAt` DATETIME NOT NULL,
  `updatedAt` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uk_users_username` (`username` ASC) VISIBLE,
  UNIQUE INDEX `uk_users_email` (`email` ASC) VISIBLE,
  UNIQUE INDEX `UKr43af9ap4edm43mmtq01oddj6` (`username` ASC) VISIBLE,
  UNIQUE INDEX `UK6dotkott2kjsp8vw4d0m25fb7` (`email` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `playlist_schema`.`user_roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `playlist_schema`.`user_roles` ;

CREATE TABLE IF NOT EXISTS `playlist_schema`.`user_roles` (
  `user_id` BIGINT(20) NOT NULL,
  `role_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`),
  INDEX `fk_user_roles_role_id` (`role_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_roles_role_id`
    FOREIGN KEY (`role_id`)
    REFERENCES `playlist_schema`.`roles` (`id`),
  CONSTRAINT `fk_user_roles_user_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `playlist_schema`.`users` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT IGNORE INTO roles(name) VALUES('ROLE_USER');
INSERT IGNORE INTO roles(name) VALUES('ROLE_ADMIN');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
