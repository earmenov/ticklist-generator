package com.telerik.tick.playlist.generator.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerik.tick.playlist.generator.dto.payload.PlaylistEditCriteria;
import com.telerik.tick.playlist.generator.dto.payload.generator.GenrePrefenerces;
import com.telerik.tick.playlist.generator.dto.payload.generator.PlaylistRequestCriteria;
import com.telerik.tick.playlist.generator.exceptions.FileStorageException;
import com.telerik.tick.playlist.generator.model.Playlist;
import com.telerik.tick.playlist.generator.model.PlaylistCard;
import com.telerik.tick.playlist.generator.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping("api/playlist")
public class PlaylistController {

    private final PlaylistService playlistService;

    @Autowired
    PlaylistController(PlaylistService playlistService) {
        this.playlistService = playlistService;
    }

    @PostMapping(value = "/generate")
    public ResponseEntity<?> generatePlaylist(@Valid @ModelAttribute PlaylistRequestCriteria playlistRequestCriteria)
            throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        List<GenrePrefenerces> asList = mapper.readValue(
                playlistRequestCriteria.getJsonGenrePreferences(), new TypeReference<>() {
                });
        playlistRequestCriteria.setGenrePreferences(asList);

        try {
            Playlist playlist = playlistService.generatePlaylist(playlistRequestCriteria);
            URI location = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .buildAndExpand(playlist.getTitle()).toUri();
            return ResponseEntity.created(location).body(playlist);
        } catch (FileStorageException e) {
            return new ResponseEntity<HttpStatus>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        }
    }

    @PutMapping(value = "/edit")
    public ResponseEntity<?> editPlaylist(@Valid PlaylistEditCriteria playlistEditCriteria, BindingResult bindingResult)
            throws JsonProcessingException, MethodArgumentNotValidException, IntrospectionException {
        if (bindingResult.hasErrors()) {
            Method method = new PropertyDescriptor(
                    Objects.requireNonNull(bindingResult.getFieldError()).getField(),
                    PlaylistEditCriteria.class).getReadMethod();

            throw new MethodArgumentNotValidException(new MethodParameter(method, -1), bindingResult);
        }

        ObjectMapper mapper = new ObjectMapper();
        List<Integer> asList = mapper.readValue(
                playlistEditCriteria.getJsonGenrePreferences(), new TypeReference<>() {
                });
        playlistEditCriteria.setGenrePreferences(asList);

        try {
            Playlist playlist = playlistService.editPlaylist(playlistEditCriteria);
            return ResponseEntity.ok(playlist);
        } catch (FileStorageException e) {
            return new ResponseEntity<HttpStatus>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
        }
    }

    @GetMapping(value = "/get-all")
    public ResponseEntity<List<Playlist>> getAllPlaylists() {
        return ResponseEntity.ok((playlistService.findAll()));
    }

    @GetMapping(value = "/count-for-user/{userId}")
    public int countForUser(@PathVariable long userId) {
        return playlistService.countAllByUserId(userId);
    }

    @DeleteMapping(value = "/delete/{playlistId}")
    public ResponseEntity<?> deletePlaylist(@PathVariable long playlistId) {
        playlistService.deletePlaylist(playlistId);
        return new ResponseEntity<HttpStatus>(HttpStatus.OK);
    }

    @GetMapping(value = "/get-all/profile/{userId}")
    public Page<PlaylistCard> findForUser(@PathVariable long userId,
                                          @RequestParam Optional<String> title,
                                          @RequestParam Optional<Integer> fromDuration,
                                          @RequestParam Optional<Integer> toDuration,
                                          @RequestParam Optional<List<String>> checkedGenres,
                                          @RequestParam Optional<String> sortBy,
                                          @RequestParam Optional<Boolean> asc,
                                          Pageable page) {

        return playlistService.findAllForUser(
                userId,
                title.orElse(""),
                fromDuration.orElse(0),
                toDuration.orElse(24),
                checkedGenres.orElse(null),
                sortBy.orElse(""),
                asc.orElse(false),
                page);
    }

    @GetMapping(value = "/guest/filter")
    public Page<PlaylistCard> fetchByFilter(@RequestParam Optional<String> title,
                                            @RequestParam Optional<Integer> fromDuration,
                                            @RequestParam Optional<Integer> toDuration,
                                            @RequestParam Optional<List<String>> checkedGenres,
                                            @RequestParam Optional<String> sortBy,
                                            @RequestParam Optional<Boolean> asc,
                                            Pageable page) {
        return playlistService.findAllByFilter(
                title.orElse(""),
                fromDuration.orElse(0),
                toDuration.orElse(24),
                checkedGenres.orElse(null),
                sortBy.orElse(""),
                asc.orElse(false),
                page);
    }

    @GetMapping(value = "/guest/show/{id}")
    public ResponseEntity<Playlist> getPlaylistById(@PathVariable long id) {
        return ResponseEntity.ok(playlistService.findById(id));
    }
}
