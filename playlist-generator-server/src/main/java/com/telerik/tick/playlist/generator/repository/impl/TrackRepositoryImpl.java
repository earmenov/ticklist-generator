package com.telerik.tick.playlist.generator.repository.impl;

import com.telerik.tick.playlist.generator.model.Track;
import com.telerik.tick.playlist.generator.repository.CustomTrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TrackRepositoryImpl implements CustomTrackRepository {

    private static final int RECORDS_PER_TRANSACTION_LIMIT = 1000;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public TrackRepositoryImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManager = entityManagerFactory.createEntityManager();
    }

    @Override
    @Transactional
    public void saveTracks(List<Track> tracks) {
        EntityTransaction tx = entityManager.getTransaction();
        int cont = 0;
        tx.begin();

        for (Track track : tracks) {
            entityManager.persist(track);
            if (++cont == RECORDS_PER_TRANSACTION_LIMIT) {
                tx.commit();
                tx.begin();
                cont = 0;
            }
        }

        tx.commit();
    }
}
