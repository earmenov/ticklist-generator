package com.telerik.tick.playlist.generator.service.impl;

import com.telerik.tick.playlist.generator.exceptions.ResourceNotFoundException;
import com.telerik.tick.playlist.generator.model.Genre;
import com.telerik.tick.playlist.generator.repository.GenreRepository;
import com.telerik.tick.playlist.generator.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {

    private final GenreRepository genreRepository;

    @Autowired
    GenreServiceImpl(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Override
    public void saveAll(List<Genre> genres) {
        for (Genre genre : genres) {
            if (!genreRepository.existsById(genre.getId())) {
                genreRepository.save(genre);
            }
        }
    }

    @Override
    public List<Genre> findAll() {
        return genreRepository.findAll();
    }

    @Override
    public List<Genre> findGenresByName(List<String> names) {
        return genreRepository.findByNameIn(names);
    }

    @Override
    public Genre findById(long genreId) {
        return genreRepository.findById(genreId).orElseThrow(() -> new ResourceNotFoundException("Genre", "id", genreId));
    }
}
