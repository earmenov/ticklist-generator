package com.telerik.tick.playlist.generator.controller;

import com.telerik.tick.playlist.generator.model.Genre;
import com.telerik.tick.playlist.generator.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/genres")
public class GenreController {

    private final GenreService genreService;

    @Autowired
    GenreController(GenreService genreService) {
        this.genreService = genreService;
    }

    @GetMapping(value = "/get-all")
    public List<Genre> getAllGenres() {
        return genreService.findAll();
    }
}
