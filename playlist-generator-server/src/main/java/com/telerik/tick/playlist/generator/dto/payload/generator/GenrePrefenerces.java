package com.telerik.tick.playlist.generator.dto.payload.generator;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class GenrePrefenerces {

    @NotNull
    private long genreId;

    @NotNull
    @Min(value = 0)
    private int percentage;

    public GenrePrefenerces () {}

    public long getGenreId() {
        return genreId;
    }

    public void setGenreId(long genreId) {
        this.genreId = genreId;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }
}
