package com.telerik.tick.playlist.generator.dto;

import com.telerik.tick.playlist.generator.model.Track;

import java.util.List;

public class TrackListDto {

    private List<Track> data;

    public TrackListDto() {
    }

    public List<Track> getData() {
        return data;
    }

    public void setData(List<Track> data) {
        this.data = data;
    }
}
