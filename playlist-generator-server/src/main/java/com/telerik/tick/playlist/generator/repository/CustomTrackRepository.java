package com.telerik.tick.playlist.generator.repository;

import com.telerik.tick.playlist.generator.model.Track;

import java.util.List;

public interface CustomTrackRepository {

    void saveTracks(List<Track> tracks);
}
