package com.telerik.tick.playlist.generator.dto;

import com.telerik.tick.playlist.generator.model.Genre;

import java.util.List;

public class GenreListDto {

    private List<Genre> data;

    public GenreListDto() {
    }

    public List<Genre> getData() {
        return data;
    }

    public void setData(List<Genre> data) {
        this.data = data;
    }
}
