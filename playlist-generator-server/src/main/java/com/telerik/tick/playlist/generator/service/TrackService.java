package com.telerik.tick.playlist.generator.service;

import com.telerik.tick.playlist.generator.model.Track;

import java.util.List;
import java.util.Set;

public interface TrackService {

    List<Track> getAll();

    Track getById(String id);

    List<Track> saveAll(Set<Track> tracks);

    Set<String> getAllIdsOfGenre(String genre);

    Set<String> getAllIds();

    List<Track> findTopTenTracks();

    List<Track> getAllByGenre(long genreId, boolean topTracks);
}
