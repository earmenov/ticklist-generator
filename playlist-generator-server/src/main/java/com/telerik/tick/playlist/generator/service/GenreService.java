package com.telerik.tick.playlist.generator.service;


import com.telerik.tick.playlist.generator.model.Genre;

import java.util.List;

public interface GenreService {
    void saveAll(List<Genre> genres);

    List<Genre> findAll();

    List<Genre> findGenresByName(List<String> names);

    Genre findById(long genreId);
}
