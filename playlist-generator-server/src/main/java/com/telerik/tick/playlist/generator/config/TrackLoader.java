package com.telerik.tick.playlist.generator.config;

import com.google.gson.Gson;
import com.telerik.tick.playlist.generator.dto.GenreListDto;
import com.telerik.tick.playlist.generator.dto.GenrePlaylistListDto;
import com.telerik.tick.playlist.generator.dto.TrackListDto;
import com.telerik.tick.playlist.generator.exceptions.HttpConnectionException;
import com.telerik.tick.playlist.generator.model.Album;
import com.telerik.tick.playlist.generator.model.Artist;
import com.telerik.tick.playlist.generator.model.Genre;
import com.telerik.tick.playlist.generator.model.Track;
import com.telerik.tick.playlist.generator.service.AlbumService;
import com.telerik.tick.playlist.generator.service.ArtistService;
import com.telerik.tick.playlist.generator.service.GenreService;
import com.telerik.tick.playlist.generator.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
public class TrackLoader {

    private static final String GENRES_LIST_URL = "https://api.deezer.com/genre";
    private static final String ALBUM_URL = "https://www.deezer.com/album/";
    private static final String PLAYLIST_OF_GENRE_URL = "https://api.deezer.com/search/playlist?q=%s";
    private static final int LOAD_TRACK_SIZE = 1000;

    private HttpClient httpClient;

    private final TrackService trackService;
    private final GenreService genreService;
    private final ArtistService artistService;
    private final AlbumService albumService;

    private Set<String> existingTracks;
    private Set<String> existingAlbums;
    private Set<String> existingArtists;

    private Set<Artist> artists;
    private Set<Album> albums;
    private Map<Genre, Set<Track>> tracksOfGenre;

    @Autowired
    TrackLoader(TrackService trackService, GenreService genreService, ArtistService artistService, AlbumService albumService) {
        this.httpClient = HttpClient.newHttpClient();

        this.trackService = trackService;
        this.genreService = genreService;
        this.artistService = artistService;
        this.albumService = albumService;

        this.existingTracks = ConcurrentHashMap.newKeySet();
        this.existingAlbums = ConcurrentHashMap.newKeySet();
        this.existingArtists = ConcurrentHashMap.newKeySet();

        this.artists = ConcurrentHashMap.newKeySet();
        this.albums = ConcurrentHashMap.newKeySet();
        this.tracksOfGenre = new ConcurrentHashMap<>();
    }

    @PostConstruct
    public void init() {
        List<Genre> genreList = retrieveGenres();
        genreService.saveAll(genreList);

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize((genreList.size() - (genreList.size() / 2)));
        executor.setMaxPoolSize(genreList.size());
        executor.initialize();

        List<CompletableFuture<Set<Track>>> fetchFutures = new ArrayList<>();
        existingTracks = trackService.getAllIds();
        existingAlbums = albumService.getAllIds();
        existingArtists = artistService.getAllIds();
        for (Genre genre : genreList) {
            fetchFutures.add(CompletableFuture.supplyAsync(() ->
                    extractSongsOfGenre(
                            genre, LOAD_TRACK_SIZE - trackService.getAllIdsOfGenre(genre.getName()).size()), executor));
        }
        CompletableFuture.allOf(fetchFutures.toArray(CompletableFuture[]::new)).join();

        List<CompletableFuture> persistFutures = new ArrayList<>();
        persistFutures.add(CompletableFuture.supplyAsync(() -> artistService.saveAll(artists)));
        persistFutures.add(CompletableFuture.supplyAsync(() -> albumService.saveAll(albums)));
        CompletableFuture.allOf(persistFutures.toArray(CompletableFuture[]::new)).join();
        tracksOfGenre.values().parallelStream().forEach(trackService::saveAll);
    }

    private List<Genre> retrieveGenres() {
        try {
            Gson gson = new Gson();
            HttpRequest request = HttpRequest.newBuilder().uri(URI.create(GENRES_LIST_URL)).build();
            String jsonResponse = httpClient.send(request, HttpResponse.BodyHandlers.ofString()).body();
            List<Genre> genreList = gson.fromJson(jsonResponse, GenreListDto.class).getData();
            return genreList.stream().filter(genre -> !genre.getName().equals("All")).collect(Collectors.toList());
        } catch (IOException | InterruptedException e) {
            throw new HttpConnectionException(e.getMessage(), e);
        }
    }


    private Set<Track> extractSongsOfGenre(Genre genre, int tracksNeeded) {
        try {
            Gson gson = new Gson();
            tracksOfGenre.put(genre, ConcurrentHashMap.newKeySet());

            HttpRequest playlistRequest = HttpRequest.newBuilder()
                    .uri(URI.create(String.format(PLAYLIST_OF_GENRE_URL, genre.getName()).replace(' ', '+'))).build();
            String playlistJsonResponse = getResponse(playlistRequest);
            if(playlistJsonResponse == null) {
                return ConcurrentHashMap.newKeySet();
            }

            GenrePlaylistListDto dto = gson.fromJson(playlistJsonResponse, GenrePlaylistListDto.class);
            int playlistPageSize = dto.getData().size();
            int playlistPageIndex = 0;

            while (tracksNeeded > 0) {
                if (playlistPageIndex == playlistPageSize) {

                    playlistJsonResponse = getResponse(playlistRequest);
                    dto = gson.fromJson(playlistJsonResponse, GenrePlaylistListDto.class);
                    if (playlistJsonResponse == null) {
                        break;
                    }

                    System.out.println(dto.getNext());
                    playlistRequest = HttpRequest.newBuilder().uri(URI.create(URLDecoder.decode(dto.getNext(), StandardCharsets.UTF_8.toString()).replace(' ', '+'))).build();
                    playlistJsonResponse = getResponse(playlistRequest);
                    if (playlistJsonResponse == null) {
                        break;
                    }

                    dto = gson.fromJson(playlistJsonResponse, GenrePlaylistListDto.class);
                    playlistPageIndex = 0;
                    playlistPageSize = dto.getData().size();
                }

                HttpRequest tracklistRequest = HttpRequest.newBuilder().uri(URI.create(dto.getData()
                        .get(playlistPageIndex++).getTracklist())).build();
                String tracklistJsonResponse = getResponse(tracklistRequest);
                if (tracklistJsonResponse == null) {
                    break;
                }

                TrackListDto trackListDto = gson.fromJson(tracklistJsonResponse, TrackListDto.class);
                tracksNeeded -= extractTracksFromPlaylist(trackListDto.getData(), genre, tracksNeeded);
            }
        } catch (IOException | InterruptedException e) {
            throw new HttpConnectionException(e.getMessage(), e);
        }
        return tracksOfGenre.get(genre);
    }

    private String getResponse(HttpRequest httpRequest) throws IOException, InterruptedException {
        String jsonResponse;
        do {
            jsonResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString()).body();
            if (jsonResponse.contains("OAuthException")) {
                return null;
            }
        } while (jsonResponse.contains("Quota limit exceeded"));

        return jsonResponse;
    }

    private int extractTracksFromPlaylist(List<Track> tracks, Genre genre, int tracksNeeded) {
        int tracksExtracted = 0;

        for (Track track : tracks) {

            if (tracksNeeded == 0) {
                break;
            }

            if (track.getId() == null || track.getAlbum().getId() == null || track.getArtist().getId() == null) {
                continue;
            }

            if (!existingTracks.contains(track.getId())) {
                track.setGenre(genre);
                existingTracks.add(track.getId());
                track.getAlbum().setLink(ALBUM_URL + track.getAlbum().getId());
                if (!existingArtists.contains(track.getArtist().getId())) {
                    existingArtists.add(track.getArtist().getId());
                    artists.add(track.getArtist());
                }
                if (!existingAlbums.contains(track.getAlbum().getId())) {
                    existingAlbums.add(track.getAlbum().getId());
                    albums.add(track.getAlbum());
                }
                tracksOfGenre.get(genre).add(track);
                tracksNeeded--;
                tracksExtracted++;
            }
        }

        return tracksExtracted;
    }
}