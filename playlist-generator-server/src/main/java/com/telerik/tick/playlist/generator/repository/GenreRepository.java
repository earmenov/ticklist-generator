package com.telerik.tick.playlist.generator.repository;

import com.telerik.tick.playlist.generator.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {

    List<Genre> findByNameIn(List<String> genreNames);
}
