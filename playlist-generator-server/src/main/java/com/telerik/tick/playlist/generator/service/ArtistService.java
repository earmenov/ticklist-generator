package com.telerik.tick.playlist.generator.service;

import com.telerik.tick.playlist.generator.model.Artist;

import java.util.List;
import java.util.Set;

public interface ArtistService {
    List<Artist> saveAll(Set<Artist> artistSet);

    Set<String> getAllIds();
}
