package com.telerik.tick.playlist.generator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "\"artist\"")
public class Artist {

    @Id
    @Column(name = "\"ID\"", nullable = false)
    private String id;

    @Column(name = "\"NAME\"", nullable = false)
    private String name;

    @Column(name = "\"TRACKLIST_URL\"", nullable = false)
    private String link;

    public Artist() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Artist artist = (Artist) o;

        return id.equals(artist.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
