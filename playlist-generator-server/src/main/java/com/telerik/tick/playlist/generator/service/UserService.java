package com.telerik.tick.playlist.generator.service;

import com.telerik.tick.playlist.generator.dto.payload.UserEditRequest;
import com.telerik.tick.playlist.generator.dto.payload.UserSummary;
import com.telerik.tick.playlist.generator.model.User;

import java.util.Optional;

public interface UserService {
    Optional<User> findByUserId(long id);

    UserSummary getUserSummary(long id);

    User updateUser(UserEditRequest userEditRequest);
}
