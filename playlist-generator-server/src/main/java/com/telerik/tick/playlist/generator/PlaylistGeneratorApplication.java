package com.telerik.tick.playlist.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlaylistGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlaylistGeneratorApplication.class, args);
    }

}
