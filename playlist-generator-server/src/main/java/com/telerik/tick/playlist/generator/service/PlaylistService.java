package com.telerik.tick.playlist.generator.service;

import com.telerik.tick.playlist.generator.dto.payload.PlaylistEditCriteria;
import com.telerik.tick.playlist.generator.dto.payload.generator.PlaylistRequestCriteria;
import com.telerik.tick.playlist.generator.model.Playlist;
import com.telerik.tick.playlist.generator.model.PlaylistCard;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface PlaylistService {

    Playlist generatePlaylist(PlaylistRequestCriteria playlistRequestCriteria);

    Playlist editPlaylist(PlaylistEditCriteria playlistEditCriteria);

    List<Playlist> findAll();

    Playlist findById(long id);

    Page<PlaylistCard> findAllForUser(long userId, String title, int fromDuration, int toDuration,
                                      List<String> genreNames, String sortBy, boolean isAscending, Pageable page);


    Page<PlaylistCard> findAllByFilter(String title, int fromDuration, int toDuration, List<String> genres, String
            sortBy, boolean isAscending, Pageable page);

    int countAllByUserId(long id);

    void deletePlaylist(long playlistId);
}
