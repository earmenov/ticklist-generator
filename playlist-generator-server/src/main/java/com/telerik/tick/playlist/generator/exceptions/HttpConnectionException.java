package com.telerik.tick.playlist.generator.exceptions;

public class HttpConnectionException extends RuntimeException {
    public HttpConnectionException(String message, Exception e) {
        super(message, e);
    }
}
