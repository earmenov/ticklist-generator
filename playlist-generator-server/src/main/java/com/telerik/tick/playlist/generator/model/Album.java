package com.telerik.tick.playlist.generator.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "\"album\"")
public class Album {

    @Id
    @Column(name = "\"ID\"", nullable = false)
    private String id;

    @Column(name = "\"NAME\"", nullable = false)
    private String title;

    @Column(name = "\"TRACKLIST_URL\"", nullable = false)
    private String link;

    public  Album() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Album album = (Album) o;

        return id.equals(album.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
