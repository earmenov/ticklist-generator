package com.telerik.tick.playlist.generator.controller;

import com.telerik.tick.playlist.generator.dto.payload.*;
import com.telerik.tick.playlist.generator.exceptions.ResourceNotFoundException;
import com.telerik.tick.playlist.generator.exceptions.UserExistsException;
import com.telerik.tick.playlist.generator.model.User;
import com.telerik.tick.playlist.generator.model.UserPrincipal;
import com.telerik.tick.playlist.generator.repository.UserRepository;
import com.telerik.tick.playlist.generator.security.CurrentUser;
import com.telerik.tick.playlist.generator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api")
public class UserController {

    private final UserRepository userRepository;
    private final UserService userService;

    @Autowired
    public UserController(UserRepository userRepository, UserService userService) {
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public UserSummary getCurrentUser(@CurrentUser UserPrincipal currentUser) {
        return new UserSummary(currentUser.getId(), currentUser.getUsername(), currentUser.getEmail(), currentUser.getAuthorities());
    }

    @GetMapping("/user/{id}")
    public UserSummary getUserById(@PathVariable long id) {
        return userService.getUserSummary(id);
    }

    @GetMapping("/user/checkUsernameAvailability")
    public UserIdentityAvailability checkUsernameAvailability(@RequestParam(value = "username") String username) {
        Boolean isAvailable = !userRepository.existsByUsername(username);
        return new UserIdentityAvailability(isAvailable);
    }

    @GetMapping("/user/checkEmailAvailability")
    public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
        Boolean isAvailable = !userRepository.existsByEmail(email);
        return new UserIdentityAvailability(isAvailable);
    }

    @GetMapping("/users/{username}")
    public UserProfile getUserProfile(@PathVariable(value = "username") String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));


        return new UserProfile(user.getId(), user.getUsername(), user.getCreatedAt());
    }

    @PutMapping("/users/update")
    public ResponseEntity<?> updateUser(@Valid @RequestBody UserEditRequest userEditRequest) {
        try {
            URI location = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .buildAndExpand().toUri();

            User user = userService.updateUser(userEditRequest);
            return ResponseEntity.created(location).body(user);
        } catch (UserExistsException | NoSuchElementException e) {
            return new ResponseEntity<>(new ApiResponse(false, e.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}
