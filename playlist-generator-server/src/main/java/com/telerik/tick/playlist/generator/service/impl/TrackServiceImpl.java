package com.telerik.tick.playlist.generator.service.impl;

import com.telerik.tick.playlist.generator.model.Genre;
import com.telerik.tick.playlist.generator.model.Track;
import com.telerik.tick.playlist.generator.repository.TrackRepository;
import com.telerik.tick.playlist.generator.service.GenreService;
import com.telerik.tick.playlist.generator.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service
public class TrackServiceImpl implements TrackService {

    private final TrackRepository trackRepository;
    private final GenreService genreService;

    @Autowired
    TrackServiceImpl(TrackRepository trackRepository, GenreService genreService) {
        this.trackRepository = trackRepository;
        this.genreService = genreService;
    }

    @Override
    public List<Track> getAll() {
        return trackRepository.findAll();
    }

    @Override
    public Track getById(String id) {
        return trackRepository.getById(id);
    }

    @Override
    public List<Track> saveAll(Set<Track> tracks) {
        return trackRepository.saveAll(tracks);
    }

    @Override
    public Set<String> getAllIdsOfGenre(String genre) {
        return trackRepository.getAllIdsOfGenre(genre);
    }

    @Override
    public Set<String> getAllIds() {
        return trackRepository.getAllIds();
    }

    @Override
    public List<Track> findTopTenTracks() {
        return new ArrayList<>();
    }

    @Override
    public List<Track> getAllByGenre(long genreId, boolean topTracks) {
        Genre genre = genreService.findById(genreId);
        if (topTracks) {
            return trackRepository.getAllByGenreOrderByRankDesc(genre);
        } else {
            List<Track> shuffledTracks = trackRepository.getAllByGenre(genre);
            Collections.shuffle(shuffledTracks);
            return shuffledTracks;
        }
    }
}
