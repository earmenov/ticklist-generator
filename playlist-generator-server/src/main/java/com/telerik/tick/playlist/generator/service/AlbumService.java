package com.telerik.tick.playlist.generator.service;

import com.telerik.tick.playlist.generator.model.Album;

import java.util.List;
import java.util.Set;

public interface AlbumService {
    List<Album> saveAll(Set<Album> artistSet);

    Set<String> getAllIds();
}
