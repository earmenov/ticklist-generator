package com.telerik.tick.playlist.generator.dto;

import java.util.List;

public class GenrePlaylistListDto {

    private List<GenrePlaylistsDto> data;

    private String next;

    public GenrePlaylistListDto() {
    }

    public List<GenrePlaylistsDto> getData() {
        return data;
    }

    public void setData(List<GenrePlaylistsDto> data) {
        this.data = data;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }
}
