package com.telerik.tick.playlist.generator.repository;

import com.telerik.tick.playlist.generator.model.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface AlbumRepository extends JpaRepository<Album, String> {

    @Query("select a.id from Album a")
    Set<String> getAllIds();
}
