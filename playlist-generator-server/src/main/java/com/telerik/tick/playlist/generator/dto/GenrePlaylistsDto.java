package com.telerik.tick.playlist.generator.dto;

public class GenrePlaylistsDto {

    private int nb_tracks;

    private String tracklist;

    public GenrePlaylistsDto() {
    }

    public int getNb_tracks() {
        return nb_tracks;
    }

    public void setNb_tracks(int nb_tracks) {
        this.nb_tracks = nb_tracks;
    }

    public String getTracklist() {
        return tracklist;
    }

    public void setTracklist(String tracklist) {
        this.tracklist = tracklist;
    }
}
