package com.telerik.tick.playlist.generator.repository;

import com.telerik.tick.playlist.generator.model.Artist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, String> {

    @Query("select a.id from Artist a")
    Set<String> getAllIds();
}
