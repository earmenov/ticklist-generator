package com.telerik.tick.playlist.generator.repository;

import com.telerik.tick.playlist.generator.model.Genre;
import com.telerik.tick.playlist.generator.model.Track;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface TrackRepository extends JpaRepository<Track, String>, CustomTrackRepository {

    @Query("select t.id from Track t join t.genre as g where g.name = :genre")
    Set<String> getAllIdsOfGenre(@Param("genre") String genre);

    @Query("select t.id from Track t")
    Set<String> getAllIds();

    Track getById(String id);

    List<Track> getAllByGenre(Genre genre);

    List<Track> getAllByGenreOrderByRankDesc(Genre genre);
}
