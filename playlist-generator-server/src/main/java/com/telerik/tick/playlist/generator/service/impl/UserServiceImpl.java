package com.telerik.tick.playlist.generator.service.impl;

import com.telerik.tick.playlist.generator.dto.payload.UserEditRequest;
import com.telerik.tick.playlist.generator.dto.payload.UserSummary;
import com.telerik.tick.playlist.generator.exceptions.UserExistsException;
import com.telerik.tick.playlist.generator.model.User;
import com.telerik.tick.playlist.generator.model.UserPrincipal;
import com.telerik.tick.playlist.generator.repository.UserRepository;
import com.telerik.tick.playlist.generator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Optional<User> findByUserId(long id) {
        return userRepository.findById(id);
    }

    @Override
    public UserSummary getUserSummary(long id) {
        UserPrincipal user = UserPrincipal.create(userRepository.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + id))
        );

        return new UserSummary(user.getId(), user.getUsername(), user.getEmail(), user.getAuthorities());
    }

    @Override
    public User updateUser(UserEditRequest userEditRequest) {
        User changeUser = findByUserId(userEditRequest.getId()).orElseThrow();

        changeUserEmail(changeUser, userEditRequest);
        changeUserName(changeUser, userEditRequest);

        if (userEditRequest.getCurrPassword() != null) {
            if (passwordEncoder.matches(userEditRequest.getCurrPassword(), changeUser.getPassword())) {
                changeUser.setPassword(passwordEncoder.encode(userEditRequest.getNewPassword()));
            }
        }

        return userRepository.save(changeUser);
    }

    private void changeUserEmail(User changeUser, UserEditRequest userEditRequest) {
        User emailUser = userRepository.findByEmail(userEditRequest.getEmail()).orElse(null);
        if (emailUser == null || emailUser.getId() == userEditRequest.getId()) {
            changeUser.setEmail(userEditRequest.getEmail());
        } else {
            throw new UserExistsException("Email is already taken");
        }
    }

    private void changeUserName(User changeUser, UserEditRequest userEditRequest) {
        User nameUser = userRepository.findByUsername(userEditRequest.getUsername()).orElse(null);
        if (nameUser == null || nameUser.getId() == userEditRequest.getId()) {
            changeUser.setUsername(userEditRequest.getUsername());
        } else {
            throw new UserExistsException("Username is already taken");
        }
    }
}
