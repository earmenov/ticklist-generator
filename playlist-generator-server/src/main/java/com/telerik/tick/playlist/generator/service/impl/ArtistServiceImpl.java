package com.telerik.tick.playlist.generator.service.impl;

import com.telerik.tick.playlist.generator.model.Artist;
import com.telerik.tick.playlist.generator.repository.ArtistRepository;
import com.telerik.tick.playlist.generator.service.ArtistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class ArtistServiceImpl implements ArtistService {

    private final ArtistRepository artistRepository;

    @Autowired
    ArtistServiceImpl(ArtistRepository artistRepository) {
        this.artistRepository = artistRepository;
    }

    @Override
    public List<Artist> saveAll(Set<Artist> artistSet) {
        return artistRepository.saveAll(artistSet);
    }

    @Override
    public Set<String> getAllIds() {
        return artistRepository.getAllIds();
    }
}
