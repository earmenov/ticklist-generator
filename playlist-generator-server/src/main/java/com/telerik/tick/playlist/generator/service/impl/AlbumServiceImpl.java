package com.telerik.tick.playlist.generator.service.impl;

import com.telerik.tick.playlist.generator.model.Album;
import com.telerik.tick.playlist.generator.repository.AlbumRepository;
import com.telerik.tick.playlist.generator.service.AlbumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class AlbumServiceImpl implements AlbumService {

    private final AlbumRepository albumRepository;

    @Autowired
    AlbumServiceImpl(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    @Override
    public List<Album> saveAll(Set<Album> artistSet) {
        return albumRepository.saveAll(artistSet);
    }

    @Override
    public Set<String> getAllIds() {
        return albumRepository.getAllIds();
    }
}
