package com.telerik.tick.playlist.generator.repository;

import com.telerik.tick.playlist.generator.model.Genre;
import com.telerik.tick.playlist.generator.model.PlaylistCard;
import com.telerik.tick.playlist.generator.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlaylistCardRepository extends JpaRepository<PlaylistCard, String> {

    Page<PlaylistCard> findAllByTitleContainingAndTotalDurationBetween
            (String title, int fromDuration, int toDuration, Pageable page);

    Page<PlaylistCard> findAllByUserAndTitleContainingAndTotalDurationBetween
            (User user, String title, int fromDuration, int toDuration, Pageable page);

    Page<PlaylistCard> findAllByTitleContainingAndTotalDurationBetweenAndGenresIn
            (String title, int fromDuration, int toDuration, List<Genre> genres, Pageable page);

    Page<PlaylistCard> findAllByUserAndTitleContainingAndTotalDurationBetweenAndGenresIn
            (User user, String title, int fromDuration, int toDuration, List<Genre> genres, Pageable page);

    @Query("select count(*) from Playlist p where p.user.id = :userId")
    int countAllByUserId(@Param("userId") long id);
}
