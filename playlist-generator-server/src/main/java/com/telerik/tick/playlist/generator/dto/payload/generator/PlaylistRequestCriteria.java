package com.telerik.tick.playlist.generator.dto.payload.generator;

import org.springframework.data.annotation.Transient;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.util.List;

public class PlaylistRequestCriteria {

    @NotNull
    private long creatorId;

    @NotBlank
    @Size(max = 60, message = "Title should not be longer than 60 symbols")
    private String playlistTitle;

    @NotNull
    @Min(value = 0)
    @Max(value = 1440)
    private double travelDuration;

    @NotNull
    private String jsonGenrePreferences;

    @NotNull
    private MultipartFile playlistImage;

    private boolean topTracks;

    private boolean sameArtist;

    @Transient
    private List<GenrePrefenerces> genrePreferences;

    public PlaylistRequestCriteria() {}

    public long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(long creatorId) {
        this.creatorId = creatorId;
    }

    public String getPlaylistTitle() {
        return playlistTitle;
    }

    public void setPlaylistTitle(String playlistTitle) {
        this.playlistTitle = playlistTitle;
    }

    public double getTravelDuration() {
        return travelDuration;
    }

    public void setTravelDuration(double travelDuration) {
        this.travelDuration = travelDuration;
    }

    public String getJsonGenrePreferences() {
        return jsonGenrePreferences;
    }

    public void setJsonGenrePreferences(String jsonGenrePreferences) {
        this.jsonGenrePreferences = jsonGenrePreferences;
    }

    public MultipartFile getPlaylistImage() {
        return playlistImage;
    }

    public void setPlaylistImage(MultipartFile playlistImage) {
        this.playlistImage = playlistImage;
    }

    public boolean isTopTracks() {
        return topTracks;
    }

    public void setTopTracks(boolean topTracks) {
        this.topTracks = topTracks;
    }

    public boolean isSameArtist() {
        return sameArtist;
    }

    public void setSameArtist(boolean sameArtist) {
        this.sameArtist = sameArtist;
    }

    public List<GenrePrefenerces> getGenrePreferences() {
        return genrePreferences;
    }

    public void setGenrePreferences(List<GenrePrefenerces> genrePreferences) {
        this.genrePreferences = genrePreferences;
    }
}
