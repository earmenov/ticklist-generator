package com.telerik.tick.playlist.generator.model;

import javax.persistence.*;

@Entity
@Table(name = "\"genre\"")
public class Genre {

    @Id
    @Column(name = "\"ID\"", nullable = false)
    private long id;

    @Column(name = "\"NAME\"", nullable = false)
    private String name;

    public Genre() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
