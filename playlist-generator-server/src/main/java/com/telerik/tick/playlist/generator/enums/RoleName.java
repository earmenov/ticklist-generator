package com.telerik.tick.playlist.generator.enums;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
