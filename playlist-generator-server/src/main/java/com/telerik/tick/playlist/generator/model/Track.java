package com.telerik.tick.playlist.generator.model;

import javax.persistence.*;

@Entity
@Table(name = "\"track\"")
public class Track {

    @Id
    @Column(name = "\"ID\"", nullable = false)
    private String id;

    @Column(name = "\"TITLE\"", nullable = false)
    private String title;

    @Column(name = "\"LINK\"", nullable = false)
    private String link;

    @Column(name = "\"DURATION\"", nullable = false)
    private String duration;

    @Column(name = "\"RANK\"", nullable = false)
    private String rank;

    @Column(name = "\"PREVIEW_URL\"")
    private String preview;

    @ManyToOne
    @JoinColumn(name="\"ALBUM_ID\"", nullable=false)
    private Album album;

    @ManyToOne
    @JoinColumn(name="\"ARTIST_ID\"", nullable=false)
    private Artist artist;

    @ManyToOne
    @JoinColumn(name="\"GENRE_ID\"", nullable=false)
    private Genre genre;

    public Track() {
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Track track = (Track) o;

        return id.equals(track.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
