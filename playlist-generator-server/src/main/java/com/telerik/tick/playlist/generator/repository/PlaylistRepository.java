package com.telerik.tick.playlist.generator.repository;

import com.telerik.tick.playlist.generator.model.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaylistRepository extends JpaRepository<Playlist, Long> {
}
