package com.telerik.tick.playlist.generator.repository;

import com.telerik.tick.playlist.generator.enums.RoleName;
import com.telerik.tick.playlist.generator.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
