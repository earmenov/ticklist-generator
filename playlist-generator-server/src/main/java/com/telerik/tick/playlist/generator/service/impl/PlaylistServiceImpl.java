package com.telerik.tick.playlist.generator.service.impl;

import com.telerik.tick.playlist.generator.dto.payload.PlaylistEditCriteria;
import com.telerik.tick.playlist.generator.dto.payload.generator.GenrePrefenerces;
import com.telerik.tick.playlist.generator.dto.payload.generator.PlaylistRequestCriteria;
import com.telerik.tick.playlist.generator.exceptions.FileStorageException;
import com.telerik.tick.playlist.generator.exceptions.ResourceNotFoundException;
import com.telerik.tick.playlist.generator.model.*;
import com.telerik.tick.playlist.generator.repository.PlaylistCardRepository;
import com.telerik.tick.playlist.generator.repository.PlaylistRepository;
import com.telerik.tick.playlist.generator.service.GenreService;
import com.telerik.tick.playlist.generator.service.PlaylistService;
import com.telerik.tick.playlist.generator.service.TrackService;
import com.telerik.tick.playlist.generator.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PlaylistServiceImpl implements PlaylistService {

    private static final double MAX_PERCENTAGE = 100;

    private final PlaylistRepository playlistRepository;
    private final PlaylistCardRepository playlistCardRepository;
    private final TrackService trackService;
    private final GenreService genreService;
    private final UserService userService;

    @Autowired
    PlaylistServiceImpl(PlaylistRepository playlistRepository, PlaylistCardRepository playlistCardRepository,
                        TrackService trackService, GenreService genreService, UserService userService) {
        this.playlistRepository = playlistRepository;
        this.playlistCardRepository = playlistCardRepository;
        this.trackService = trackService;
        this.genreService = genreService;
        this.userService = userService;
    }

    @Override
    public Playlist generatePlaylist(PlaylistRequestCriteria playlistRequestCriteria) {
        Playlist playlist = new Playlist();
        Set<Track> playlistTracks = new HashSet<>();
        Set<Genre> playlistGenres = new HashSet<>();

        for (GenrePrefenerces preference : playlistRequestCriteria.getGenrePreferences()) {
            double travelGenreDuration = playlistRequestCriteria.getTravelDuration() * (preference.getPercentage() / MAX_PERCENTAGE);

            List<Track> genreTracks = trackService.getAllByGenre(preference.getGenreId(), playlistRequestCriteria.isTopTracks());
            addTracksToPlaylist(genreTracks, playlistTracks, travelGenreDuration, playlistRequestCriteria.isSameArtist());
            playlistGenres.add(genreService.findById(preference.getGenreId()));
        }

        try {
            playlist.setCoverImage(playlistRequestCriteria.getPlaylistImage().getBytes());
        } catch (IOException ex) {
            String fileName = StringUtils.cleanPath(playlistRequestCriteria.getPlaylistImage().getOriginalFilename());
            throw new FileStorageException("Playlist cover image " + fileName + " may be corrupted. Please try again!", ex);
        }

        setRankAndDuration(playlistTracks, playlist);
        playlist.setTitle(playlistRequestCriteria.getPlaylistTitle());
        playlist.setTracks(playlistTracks);
        playlist.setGenres(playlistGenres);
        playlist.setUser(userService.findByUserId(playlistRequestCriteria.getCreatorId()).orElseThrow());

        return playlistRepository.save(playlist);
    }

    @Override
    public Playlist editPlaylist(PlaylistEditCriteria playlistEditCriteria) {
        Playlist playlist = findById(playlistEditCriteria.getId());

        if (playlistEditCriteria.getGenrePreferences() != null) {
            Set<Track> playlistTracks = new HashSet<>();
            int i = 0;
            double playlistDuration = playlist.getTotalDuration() / 60d;
            for (Genre genre : playlist.getGenres()) {
                double travelGenreDuration = playlistDuration * (playlistEditCriteria.getGenrePreferences().get(i++) / MAX_PERCENTAGE);

                List<Track> genreTracks = trackService.getAllByGenre(genre.getId(), false);
                addTracksToPlaylist(genreTracks, playlistTracks, travelGenreDuration, false);
            }

            playlist.setTracks(playlistTracks);
            setRankAndDuration(playlistTracks, playlist);
        }

        playlist.setTitle(playlistEditCriteria.getTitle());

        if (playlistEditCriteria.getImage() != null) {
            try {
                playlist.setCoverImage(playlistEditCriteria.getImage().orElseThrow().getBytes());
            } catch (IOException ex) {
                String fileName = StringUtils.cleanPath(playlistEditCriteria.getImage().orElseThrow().getOriginalFilename());
                throw new FileStorageException("Playlist cover image " + fileName + " may be corrupted. Please try again!", ex);
            }
        }

        return playlistRepository.save(playlist);
    }

    @Override
    public List<Playlist> findAll() {
        return playlistRepository.findAll();
    }

    @Override
    public Playlist findById(long id) {
        return playlistRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Playlist", "id", id));
    }

    @Override
    public Page<PlaylistCard> findAllForUser(long userId, String title, int fromDuration, int toDuration, List<String> genreNames, String sortBy, boolean isAscending, Pageable page) {
        User user = userService.findByUserId(userId).orElseThrow();
        fromDuration *= 60 * 60;
        toDuration *= 60 * 60;

        if (!sortBy.isEmpty()) {
            page = PageRequest.of(page.getPageNumber(), page.getPageSize(), createSortRequest(sortBy, isAscending));
        }

        if (genreNames != null) {
            List<Genre> genres = genreService.findGenresByName(genreNames);
            return playlistCardRepository.findAllByUserAndTitleContainingAndTotalDurationBetweenAndGenresIn(user, title, fromDuration, toDuration, genres, page);
        } else {
            return playlistCardRepository.findAllByUserAndTitleContainingAndTotalDurationBetween(user, title, fromDuration, toDuration, page);
        }
    }

    @Override
    public Page<PlaylistCard> findAllByFilter(String title, int fromDuration, int toDuration, List<String> genreNames, String sortBy, boolean isAscending, Pageable page) {
        fromDuration *= 60 * 60;
        toDuration *= 60 * 60;

        if (!sortBy.isEmpty()) {
            page = PageRequest.of(page.getPageNumber(), page.getPageSize(), createSortRequest(sortBy, isAscending));
        }

        if (genreNames != null) {
            List<Genre> genres = genreService.findGenresByName(genreNames);
            return playlistCardRepository.findAllByTitleContainingAndTotalDurationBetweenAndGenresIn(title, fromDuration, toDuration, genres, page);
        } else {
            return playlistCardRepository.findAllByTitleContainingAndTotalDurationBetween(title, fromDuration, toDuration, page);
        }
    }

    @Override
    public int countAllByUserId(long id) {
        return playlistCardRepository.countAllByUserId(id);
    }

    @Override
    public void deletePlaylist(long playlistId) {
        playlistRepository.delete(findById(playlistId));
    }

    private Sort createSortRequest(String sortBy, boolean isAscending) {
        return isAscending ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
    }

    private void addTracksToPlaylist(List<Track> genreTracks, Set<Track> playlistTracks, double travelGenreDuration, boolean sameArtist) {
        if (sameArtist) {
            for (Track track : genreTracks) {
                if (travelGenreDuration < 0) {
                    break;
                }

                playlistTracks.add(track);
                travelGenreDuration -= Double.parseDouble(track.getDuration()) / 60d;

            }
        } else {
            Set<String> includedArtists = new HashSet<>();

            for (Track track : genreTracks) {
                if (travelGenreDuration < 0) {
                    break;
                }

                if (!includedArtists.contains(track.getArtist().getId())) {
                    includedArtists.add(track.getArtist().getId());
                    playlistTracks.add(track);
                    travelGenreDuration -= Double.parseDouble(track.getDuration()) / 60d;
                }
            }
        }
    }

    private void setRankAndDuration(Set<Track> playlistTracks, Playlist playlist) {
        int totalDuration = 0;
        int playlistRank = 0;

        for (Track playlistTrack : playlistTracks) {
            totalDuration += Integer.parseInt(playlistTrack.getDuration());
            playlistRank += Integer.parseInt(playlistTrack.getRank());
        }

        int avgRank = playlistRank / playlistTracks.size();

        playlist.setAvgRank(avgRank);
        playlist.setTotalDuration(totalDuration);
    }
}
