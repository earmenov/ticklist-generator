package com.telerik.tick.playlist.generator.dto.payload;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserEditRequest {

    @NotNull
    private long id;

    @NotBlank
    @Size(min = 5, max = 20, message = "Username must be between 5 and 20 symbols")
    private String username;

    @NotBlank
    @Size(max = 60)
    @Email
    private String email;

    private String currPassword;

    private String newPassword;

    public UserEditRequest() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCurrPassword() {
        return currPassword;
    }

    public void setCurrPassword(String currPassword) {
        this.currPassword = currPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
