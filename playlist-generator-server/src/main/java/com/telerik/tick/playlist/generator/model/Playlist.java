package com.telerik.tick.playlist.generator.model;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "\"playlist\"")
public class Playlist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "\"ID\"", nullable = false)
    private long id;

    @Size(max = 60)
    @Column(name = "\"TITLE\"", nullable = false)
    private String title;

    @Lob
    @Column(name = "\"COVER_IMAGE\"", nullable = false)
    private byte[] coverImage;

    @Column(name = "\"TOTAL_DURATION\"")
    private int totalDuration;

    @Column(name = "\"AVG_RANK\"")
    private int avgRank;

    @ManyToOne
    @JoinColumn(name="\"USER_ID\"", nullable=false)
    private User user;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "\"playlist_has_track\"",
            joinColumns = { @JoinColumn(name = "PLAYLIST_ID") },
            inverseJoinColumns = { @JoinColumn(name = "TRACK_ID") }
    )
    private Set<Track> tracks = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "\"playlist_has_genre\"",
            joinColumns = { @JoinColumn(name = "PLAYLIST_ID") },
            inverseJoinColumns = { @JoinColumn(name = "GENRE_ID") }
    )
    private Set<Genre> genres = new HashSet<>();

    public Playlist() {

    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public byte[] getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(byte[] coverImage) {
        this.coverImage = coverImage;
    }

    public int getTotalDuration() {
        return totalDuration;
    }

    public void setTotalDuration(int totalDuration) {
        this.totalDuration = totalDuration;
    }

    public int getAvgRank() {
        return avgRank;
    }

    public void setAvgRank(int avgRank) {
        this.avgRank = avgRank;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Track> getTracks() {
        return tracks;
    }

    public void setTracks(Set<Track> tracks) {
        this.tracks = tracks;
    }

    public Set<Genre> getGenres() {
        return genres;
    }

    public void setGenres(Set<Genre> genres) {
        this.genres = genres;
    }
}
