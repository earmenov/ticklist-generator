package com.telerik.tick.playlist.generator.dto.payload;

import org.springframework.data.annotation.Transient;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.util.List;
import java.util.Optional;

public class PlaylistEditCriteria {

    @NotNull
    private long id;

    @NotBlank
    @Size(max = 60, message = "Title should not be longer than 60 symbols")
    private String title;

    private Optional<MultipartFile> image;

    private String jsonGenrePreferences;

    @Transient
    private List<Integer> genrePreferences;

    public PlaylistEditCriteria() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Optional<MultipartFile> getImage() {
        return image;
    }

    public void setImage(Optional<MultipartFile> image) {
        this.image = image;
    }

    public String getJsonGenrePreferences() {
        return jsonGenrePreferences;
    }

    public void setJsonGenrePreferences(String jsonGenrePreferences) {
        this.jsonGenrePreferences = jsonGenrePreferences;
    }

    public List<Integer> getGenrePreferences() {
        return genrePreferences;
    }

    public void setGenrePreferences(List<Integer> genrePreferences) {
        this.genrePreferences = genrePreferences;
    }
}
