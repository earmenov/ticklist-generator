import React from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import LandingPage from "./components/LandingPage";
import PlaylistGeneratorForm from "./components/generator/PlaylistGeneratorForm";
import AuthRoute from "./components/AuthRoute";
import UnauthRoute from "./components/UnauthRoute";
import "./App.css";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import MyProfile from "./components/MyProfile";
import UserProfile from "./components/UserProfile";
import BrowsePage from "./components/BrowsePage";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#D12608",
    },
    secondary: {
      main: "#C70039",
    },
  },
});

function App() {
  return (
    <Router>
      <div>
        <MuiThemeProvider theme={theme}>
          <Switch>
            <UnauthRoute path="/" component={LandingPage} exact />
            <AuthRoute path="/home" component={PlaylistGeneratorForm} />
            <AuthRoute path="/playlists/browse" component={BrowsePage} />
            <AuthRoute path="/my-profile" component={MyProfile} />
            <AuthRoute path="/user/show/:id" component={UserProfile} exact />
          </Switch>
        </MuiThemeProvider>
      </div>
    </Router>
  );
}

export default App;
