import { useEffect, useState } from "react";
import axios from "axios";

export default function usePlaylistGuestSearch(address, pageNumber) {
  const [loading, setLoading] = useState(true);
  const [playlists, setPlaylists] = useState([]);
  const [hasMore, setHasMore] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      let cancel;
      await axios({
        method: "GET",
        url: address,
        cancelToken: new axios.CancelToken((c) => (cancel = c)),
      })
        .then((res) => {
          setLoading(false);
          setHasMore(!res.data.last);
          if (pageNumber === 0) {
            setPlaylists([]);
          }
          setPlaylists((prevPlaylists) => {
            return [...prevPlaylists, ...res.data.content];
          });
        })
        .catch((e) => {
          if (axios.isCancel(e)) return;
        });
      return () => cancel();
    };

    fetchData();
  }, [address, pageNumber]);

  return { loading, playlists, hasMore };
}
