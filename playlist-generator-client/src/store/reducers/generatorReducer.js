import DefaultPlaylistPic from "../../default-playlist-img.jpg";
import { PROGRESS_BAR_MAX_VAL } from "../../constants";

export const INIT_GENRES = "INIT_GENRES";
export const HANDLE_GENRE_CHANGE = "HANDLE_GENRE_CHANGE";
export const HANDLE_PLAYLIST_TITLE = "HANDLE_PLAYLIST_TITLE";
export const HANDLE_ORIGIN_ADDRESS = "HANDLE_ORIGIN_ADDRESS";
export const HANDLE_DESTINATION_ADDRESS = "HANDLE_DESTINATION_ADDRESS";
export const SWITCH_PREVIOUS_FORM = "SWITCH_PREVIOUS_FORM";
export const SWITCH_NEXT_FORM = "SWITCH_NEXT_FORM";
export const HANDLE_PERCENTAGE_INPUT = "HANDLE_PERCENTAGE_INPUT";
export const HANDLE_TOP_TRACKS = "HANDLE_TOP_TRACKS";
export const HANDLE_SAME_ARTIST = "HANDLE_SAME_ARTIST";
export const CHANGE_PLAYLIST_COVER = "CHANGE_PLAYLIST_COVER";
export const SET_ORIGIN_COORDS = "SET_ORIGIN_COORDS";
export const SET_COORDS = "SET_COORDS";
export const VALIDATE_ADDRESSES = "VALIDATE_ADDRESSES";
export const SET_DESTINATION_ADDRESS_ERROR = "SET_DESTINATION_ADDRESS_ERROR";

const handleCheckboxChange = (genre, state) => {
  state.checkedGenres.has(genre)
    ? state.checkedGenres.delete(genre)
    : state.checkedGenres.set(genre, 0);
  return {
    ...state,
    buttonNextDisabled:
      !state.originAddress ||
      !state.destinationAddress ||
      state.checkedGenres.size === 0,
  };
};

const switchNextForm = (state) => {
  if (
    state.errors.originAddressError.length === 0 &&
    state.errors.destinationAddressError.length === 0
  ) {
    if (state.checkedGenres.size === 1) {
      state.progressValue = PROGRESS_BAR_MAX_VAL;
      for (const k of state.checkedGenres.keys()) {
        state.checkedGenres.set(k, PROGRESS_BAR_MAX_VAL);
      }
    }

    return { ...state, nextStep: true, buttonGenerateDisabled: true };
  }

  return { ...state, nextStep: false };
};

const handlePercentageInput = (value, index, state) => {
  let i = 0;
  let newProgressSum = 0;
  for (const [k, v] of state.checkedGenres) {
    if (i === index) {
      state.checkedGenres.set(k, Number(value));
      newProgressSum += Number(value);
    } else {
      newProgressSum += v;
    }
    i++;
  }

  if (newProgressSum <= PROGRESS_BAR_MAX_VAL) {
    state.progressValue = newProgressSum;
  }

  return {
    ...state,
    buttonGenerateDisabled:
      !state.playlistTitle ||
      newProgressSum !== PROGRESS_BAR_MAX_VAL ||
      state.image === null,
  };
};

const setCoords = (coords, isAddressOrigin, state) => {
  if (isAddressOrigin) {
    return {
      ...state,
      originCoords: coords,
    };
  } else {
    return {
      ...state,
      destinationCoords: coords,
    };
  }
};

const validateAddresses = (addressErrors, state) => {
  state.errors.originAddressError = addressErrors.originError;
  state.errors.destinationAddressError = addressErrors.destinationError;

  return { ...state };
};

const changePlaylistCover = (file, state) => {
  if (file !== undefined) {
    return {
      ...state,
      imagePreview: URL.createObjectURL(file),
      image: file,
      buttonGenerateDisabled:
        !state.playlistTitle || state.progressValue !== PROGRESS_BAR_MAX_VAL,
    };
  }
};

export const generatorReducer = (state, action) => {
  switch (action.type) {
    case INIT_GENRES:
      return { ...state, genres: action.fetchedGenres };
    case HANDLE_GENRE_CHANGE:
      return handleCheckboxChange(action.genre, state);
    case SET_COORDS:
      return setCoords(action.coords, action.isAddressOrigin, state);
    case HANDLE_ORIGIN_ADDRESS:
      return {
        ...state,
        originAddress: action.value,
        buttonNextDisabled:
          !action.value ||
          !state.destinationAddress ||
          state.checkedGenres.size === 0,
      };
    case HANDLE_DESTINATION_ADDRESS:
      return {
        ...state,
        destinationAddress: action.value,
        buttonNextDisabled:
          !state.originAddress ||
          !action.value ||
          state.checkedGenres.size === 0,
      };
    case VALIDATE_ADDRESSES:
      return validateAddresses(action.addressErrors, state);
    case HANDLE_PLAYLIST_TITLE:
      return {
        ...state,
        playlistTitle: action.value,
        buttonGenerateDisabled:
          !action.value ||
          state.progressValue !== PROGRESS_BAR_MAX_VAL ||
          state.image === null,
      };
    case SWITCH_NEXT_FORM:
      return switchNextForm(state);
    case SWITCH_PREVIOUS_FORM:
      return {
        ...state,
        checkedGenres: new Map(),
        originAddress: "",
        destinationAddress: "",
        playlistTitle: "",
        imagePreview: DefaultPlaylistPic,
        image: null,
        nextStep: false,
        buttonNextDisabled: true,
        progressValue: 0,
      };
    case CHANGE_PLAYLIST_COVER:
      return changePlaylistCover(action.file, state);
    case HANDLE_PERCENTAGE_INPUT:
      return handlePercentageInput(action.value, action.index, state);
    case HANDLE_TOP_TRACKS:
      return {
        ...state,
        topTracks: !state.topTracks,
      };
    case HANDLE_SAME_ARTIST:
      return {
        ...state,
        sameArtist: !state.sameArtist,
      };
    default:
      return state;
  }
};
