const proxy = require("http-proxy-middleware");

module.exports = app => {
  app.use(
    "/REST/v1",
    proxy({
      target: "http://dev.virtualearth.net",
      changeOrigin: true
    }),
    // "/api/auth",
    // proxy({
    //   target: "http://dev.virtualearth.net",
    //   changeOrigin: true
    // })
  );
};