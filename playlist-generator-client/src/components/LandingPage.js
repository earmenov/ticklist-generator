import React from "react";
import SignUpInTransition from "./registration/SignUpInTransition";
import PlaylistDisplayer from "./PlaylistDisplayer";

const LandingPage = () => {
  return (
    <div style={{ background: "#353535", padding: 1 }}>
      <SignUpInTransition></SignUpInTransition>
      <PlaylistDisplayer isEditable={false}></PlaylistDisplayer>
    </div>
  );
};

export default LandingPage;
