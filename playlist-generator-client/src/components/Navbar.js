import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import cookie from "js-cookie";
import axios from "axios";
import UserEditForm from "./UserEditForm";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import Tooltip from "@material-ui/core/Tooltip";
import Zoom from "@material-ui/core/Zoom";
import Modal from "@material-ui/core/Modal";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import MenuIcon from "@material-ui/icons/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import LibraryAddRoundedIcon from "@material-ui/icons/LibraryAddRounded";
import LibraryMusicRoundedIcon from "@material-ui/icons/LibraryMusicRounded";
import VisibilityIcon from "@material-ui/icons/Visibility";
import FaceRoundedIcon from "@material-ui/icons/FaceRounded";
import PeopleOutlineRoundedIcon from "@material-ui/icons/PeopleOutlineRounded";
import ExitToAppRoundedIcon from "@material-ui/icons/ExitToAppRounded";

const Navbar = () => {
  const currUser = useSelector((state) => state.auth.user);

  const dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState(null);
  const [editModalOpen, setEditModalOpen] = useState(false);

  const handleLogout = () => {
    cookie.remove("token");
    dispatch({ type: "SET_LOGOUT", payload: {} });
  };

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleMyAccountOpen = () => {
    history.push("/my-profile");
  };

  const handleEditModalOpen = () => {
    setEditModalOpen(true);
  };

  const handleEditModalClose = () => {
    setEditModalOpen(false);
    handleMenuClose();
  };

  const handleGeneratorIcon = () => {
    let token = cookie.get("token");

    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    axios
      .get("/api/user/me")
      .then(() => {
        history.push("/home");
      })
      .catch(() => {
        cookie.remove("token");
        token = null;
        window.location.reload();
      });
  };

  const handleBrowseIcon = () => {
    let token = cookie.get("token");

    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    axios
      .get("/api/user/me")
      .then(() => {
        history.push("/playlists/browse");
      })
      .catch(() => {
        cookie.remove("token");
        token = null;
        window.location.reload();
      });
  };

  const renderMenu = (
    <Menu
      TransitionComponent={Zoom}
      anchorEl={anchorEl}
      open={Boolean(anchorEl)}
      onClose={handleMenuClose}
      style={{ marginTop: 55 }}
    >
      <MenuItem onClick={handleMyAccountOpen}>
        <ListItemIcon>
          <FaceRoundedIcon />
        </ListItemIcon>
        <ListItemText disableTypography={true} primary="My Account" />
      </MenuItem>
      <MenuItem onClick={handleEditModalOpen}>
        <ListItemIcon>
          <PeopleOutlineRoundedIcon />
        </ListItemIcon>
        <ListItemText disableTypography={true} primary="Edit Profile" />
      </MenuItem>
      <MenuItem onClick={handleLogout}>
        <ListItemIcon>
          <ExitToAppRoundedIcon />
        </ListItemIcon>
        <ListItemText disableTypography={true} primary="Logout" />
      </MenuItem>
    </Menu>
  );

  return (
    <div>
      <AppBar color="secondary">
        <Toolbar>
          {currUser.authorities.some((e) => e.authority === "ROLE_ADMIN") ? (
            <Tooltip arrow title="Admin Page" TransitionComponent={Zoom}>
              <IconButton
                color="inherit"
                onClick={handleBrowseIcon}
                className={classes.menuButton}
              >
                <VisibilityIcon />
              </IconButton>
            </Tooltip>
          ) : (
            <IconButton
              edge="start"
              className={classes.menuButton}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
          )}
          <Typography className={classes.title} variant="h6">
            Playlist Generator
          </Typography>
          <div className={classes.grow} />
          <div className={classes.optionSection}>
            <Tooltip
              arrow
              title="Generate New Playlist"
              TransitionComponent={Zoom}
            >
              <IconButton color="inherit" onClick={handleGeneratorIcon}>
                <LibraryAddRoundedIcon fontSize="large" />
              </IconButton>
            </Tooltip>
            <Tooltip arrow title="Browse Playlists" TransitionComponent={Zoom}>
              <IconButton color="inherit" onClick={handleBrowseIcon}>
                <LibraryMusicRoundedIcon fontSize="large" />
              </IconButton>
            </Tooltip>
            <Tooltip arrow title="Profile Options" TransitionComponent={Zoom}>
              <IconButton color="inherit" onClick={handleProfileMenuOpen}>
                <AccountCircle fontSize="large" />
              </IconButton>
            </Tooltip>
          </div>
        </Toolbar>
      </AppBar>
      <Modal
        className={classes.modal}
        open={editModalOpen}
        onClose={handleEditModalClose}
        closeAfterTransition
        disableScrollLock={true}
      >
        <UserEditForm closeModal={handleEditModalClose}></UserEditForm>
      </Modal>
      {renderMenu}
    </div>
  );
};

const useStyles = makeStyles(() => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: 16,
  },
  title: {
    display: "block",
  },
  optionSection: {
    display: "flex",
    marginRight: 25,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

export default Navbar;
