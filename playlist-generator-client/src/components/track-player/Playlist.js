import React, { useEffect, useState } from "react";
import axios from "axios";
import TracksTable from "./TracksTable";
import PlaylistInfo from "./PlaylistInfo";

const Playlist = React.forwardRef((props, ref) => {
  const [playlist, setPlaylist] = useState({});
  ref={ref}

  useEffect(() => {
    axios
      .get(`/api/playlist/guest/show/${props.playlistId}`)
      .then(res => {
        setPlaylist(res.data);
      });
    // .catch(() => {
        // todo:
    // });
  }, [props.playlistId]);

  return (
    <div style={{ backgroundColor: "#353535" }}>
      {playlist && <PlaylistInfo playlist={playlist}></PlaylistInfo>}
      <TracksTable tracks={playlist.tracks}></TracksTable>
    </div>
  );
});

export default Playlist;
