import React, {
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle
} from "react";
import convert from "convert-seconds";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import SkipPreviousIcon from "@material-ui/icons/SkipPrevious";
import PlayCircleOutlineIcon from "@material-ui/icons/PlayCircleOutline";
import SkipNextIcon from "@material-ui/icons/SkipNext";
import PauseCircleOutlineRoundedIcon from "@material-ui/icons/PauseCircleOutlineRounded";
import Link from "@material-ui/core/Link";
import Tooltip from "@material-ui/core/Tooltip";
import Zoom from "@material-ui/core/Zoom";
import Slider from "@material-ui/core/Slider";
import VolumeUpRoundedIcon from "@material-ui/icons/VolumeUpRounded";
import VolumeOffRoundedIcon from "@material-ui/icons/VolumeOffRounded";
import VolumeDownRoundedIcon from "@material-ui/icons/VolumeDownRounded";
import VolumeMuteRoundedIcon from "@material-ui/icons/VolumeMuteRounded";

const TrackPlayer = forwardRef((props, ref) => {
  const classes = useStyles();

  const [trackVolume, setTrackVolume] = useState(1);
  const [currTrackSeek, setCurrTrackSeek] = useState(0);

  useEffect(() => {
    if (!props.disablePlay && props.isTrackPlaying) {
      const interval = setInterval(() => {
        setCurrTrackSeek(currentSeek => currentSeek + 1);
      }, 1000);

      return () => clearInterval(interval);
    }
  }, [props.isTrackPlaying, props.disablePlay]);

  useImperativeHandle(ref, () => ({
    resetTrackSeek() {
      setCurrTrackSeek(0);
    }
  }));

  const convertTrackDuration = duration => {
    if (!isNaN(duration)) {
      const minutes = convert(duration).minutes;
      const seconds = convert(duration).seconds;

      return seconds >= 0 && seconds <= 9
        ? `${minutes}:0${seconds}`
        : `${minutes}:${seconds}`;
    }

    return "0:00";
  };

  const convertLongTitles = title => {
    return title.length > 20 ? `${title.substring(0, 20)}...` : title;
  };

  const handleVolumeChange = (event, value) => {
    setTrackVolume(value);
    props.changeTrackVolume(value);
  };

  const handleTrackSeek = (event, value) => {
    setCurrTrackSeek(value);
    props.changeTrackSeek(value);
  };

  const showTrackVolumeIcon = () => {
    if (trackVolume === 0) {
      return (
        <VolumeOffRoundedIcon
          className={classes.volumeIcon}
        ></VolumeOffRoundedIcon>
      );
    } else if (trackVolume >= 0.01 && trackVolume < 0.2) {
      return (
        <VolumeMuteRoundedIcon
          className={classes.volumeIcon}
        ></VolumeMuteRoundedIcon>
      );
    } else if (trackVolume >= 0.2 && trackVolume < 0.6) {
      return (
        <VolumeDownRoundedIcon
          className={classes.volumeIcon}
        ></VolumeDownRoundedIcon>
      );
    } else {
      return (
        <VolumeUpRoundedIcon
          className={classes.volumeIcon}
        ></VolumeUpRoundedIcon>
      );
    }
  };

  return (
    <div style={{ textAlign: "center", marginBottom: 30 }}>
      <Card className={classes.root}>
        <CardContent className={classes.content}>
          <Typography
            style={{ color: "whitesmoke" }}
            component="h5"
            variant="h5"
          >
            <Tooltip
              TransitionComponent={Zoom}
              arrow
              placement="top"
              enterDelay={600}
              title={props.currentTrack.title}
            >
              <Link color="inherit" underline="none">
                {convertLongTitles(props.currentTrack.title)}
              </Link>
            </Tooltip>
          </Typography>
          <Typography style={{ color: "#888888" }} variant="subtitle1">
            <Tooltip
              TransitionComponent={Zoom}
              arrow
              enterDelay={600}
              title={props.currentTrack.artist.name}
            >
              <Link color="inherit" underline="none">
                {convertLongTitles(props.currentTrack.artist.name)}
              </Link>
            </Tooltip>
          </Typography>
        </CardContent>
        <div className={classes.controls}>
          <IconButton
            onClick={() => {
              setCurrTrackSeek(0);
              props.skipPrevSong();
            }}
          >
            {<SkipPreviousIcon className={classes.skipIcon} />}
          </IconButton>
          <IconButton
            disabled={props.disablePlay}
            onClick={() => {
              props.pauseSong();
            }}
          >
            {props.isTrackPlaying ? (
              <PauseCircleOutlineRoundedIcon className={classes.playIcon} />
            ) : (
              <PlayCircleOutlineIcon className={classes.playIcon} />
            )}
          </IconButton>
          <IconButton
            onClick={() => {
              setCurrTrackSeek(0);
              props.skipNextSong();
            }}
          >
            {<SkipNextIcon className={classes.skipIcon} />}
          </IconButton>
        </div>
        {showTrackVolumeIcon()}
        <VolumeSlider
          min={0.0}
          max={1.0}
          step={0.01}
          value={trackVolume}
          onChange={handleVolumeChange}
        />
        <div style={{ alignSelf: "flex-end", position: "absolute" }}>
          <span
            style={{
              verticalAlign: "super",
              color: "#dfdfdf",
              marginRight: 16
            }}
          >
            {convertTrackDuration(currTrackSeek)}
          </span>
          <TrackDurationDisplay
            min={0}
            max={31}
            step={1}
            disabled={props.disablePlay}
            value={currTrackSeek}
            onChangeCommitted={handleTrackSeek}
          ></TrackDurationDisplay>
          <span
            style={{
              verticalAlign: "super",
              color: "#dfdfdf",
              marginLeft: 16
            }}
          >
            0:31
          </span>
        </div>
      </Card>
    </div>
  );
});

const VolumeSlider = withStyles({
  root: {
    width: 100,
    alignSelf: "center",
    color: "whitesmoke",
    "&:focus,&:hover": {
      color: "#C70039"
    }
  },
  thumb: {
    height: 12,
    width: 12,
    backgroundColor: "whitesmoke",
    marginTop: -3.5,
    "&:focus,&:hover": {
      boxShadow: "inherit"
    }
  },
  track: {
    height: 5,
    borderRadius: 4
  },
  rail: {
    height: 5,
    borderRadius: 4
  }
})(Slider);

const TrackDurationDisplay = withStyles({
  root: {
    width: 450,
    height: 0,
    color: "whitesmoke",
    "&:focus,&:hover": {
      color: "#C70039"
    }
  },
  thumb: {
    height: 10,
    width: 10,
    backgroundColor: "whitesmoke",
    marginRight: 2,
    marginTop: -2.6,
    "&:focus,&:hover": {
      boxShadow: "inherit"
    }
  },
  track: {
    height: 5,
    borderRadius: 4
  },
  rail: {
    height: 5,
    borderRadius: 4
  }
})(Slider);

const useStyles = makeStyles(() => ({
  root: {
    display: "inline-flex",
    backgroundColor: "#202020",
    justifyContent: "center",
    position: "relative",
    width: "70%",
    height: 115
  },
  content: {
    alignSelf: "center",
    justifyContent: "left"
  },
  controls: {
    alignSelf: "center",
    padding: 16
  },
  playIcon: {
    height: 38,
    width: 38,
    color: "whitesmoke",
    "&:focus,&:hover": {
      color: "#C70039"
    }
  },
  skipIcon: {
    height: 32,
    width: 32,
    color: "#888888",
    "&:focus,&:hover": {
      color: "#C70039"
    }
  },
  volumeIcon: {
    color: "#888888",
    alignSelf: "center",
    marginRight: 15
  }
}));

export default TrackPlayer;
