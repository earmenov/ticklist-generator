import React, { useState, useRef, useEffect } from "react";
import convert from "convert-seconds";
import TrackPlayer from "./TrackPlayer";
import { Howl } from "howler";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableContainer from "@material-ui/core/TableContainer";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import QueryBuilderRoundedIcon from "@material-ui/icons/QueryBuilderRounded";
import Box from "@material-ui/core/Box";
import PlayCircleFilledWhiteOutlinedIcon from "@material-ui/icons/PlayCircleFilledWhiteOutlined";
import PauseCircleOutlineRoundedIcon from "@material-ui/icons/PauseCircleOutlineRounded";
import IconButton from "@material-ui/core/IconButton";
import Link from "@material-ui/core/Link";
import Tooltip from "@material-ui/core/Tooltip";
import Zoom from "@material-ui/core/Zoom";
import Skeleton from "@material-ui/lab/Skeleton";

var sound = null;

const TracksTable = (props) => {
  const [selectedRow, setSelectedRow] = useState(null);
  const [selectedTrack, setSelectedTrack] = useState(null);
  const [disablePlayButton, setDisablePlayButton] = useState(false);
  const [isTrackPlaying, setIsTrackPlaying] = useState(false);

  const classes = useStyles();
  const childRef = useRef();

  useEffect(() => {
    return function stopTrack() {
      if (sound != null) {
        sound.stop();
        sound.unload();
        sound = null;
      }
    };
  }, []);

  const playTrack = (currRowIndex, trackSrc) => {
    let soundVolume = 1;
    if (sound != null) {
      soundVolume = sound.volume();
      sound.stop();
      sound.unload();
      sound = null;
    }
    sound = new Howl({
      src: trackSrc,
      onend: function () {
        childRef.current.resetTrackSeek();
        handleNextSkip(currRowIndex);
      },
      onloaderror: function () {
        childRef.current.resetTrackSeek();
        handleNextSkip(currRowIndex);
      },
      onplayerror: function () {
        childRef.current.resetTrackSeek();
        handleNextSkip(currRowIndex);
      },
    });
    sound.volume(soundVolume);
    sound.play();
  };

  const handlePlayClick = (rowIndex, track) => {
    if (rowIndex === selectedRow) {
      sound.playing() ? sound.pause() : sound.play();
      setIsTrackPlaying(sound.playing());
    } else {
      setSelectedRow(rowIndex);
      setSelectedTrack(track);
      setDisablePlayButton(!track.preview);
      setIsTrackPlaying(track.preview);
      track.preview && playTrack(rowIndex, track.preview);
    }
  };

  const handleVolumeChange = (volume) => {
    sound.volume(volume);
  };

  const handleTrackSeek = (at) => {
    sound.seek(at);
  };

  const handlePrevSkip = (currRow) => {
    sound.stop();
    selectedRow - 1 >= 0
      ? handlePlayClick(currRow - 1, props.tracks[currRow - 1])
      : handlePlayClick(currRow, props.tracks[currRow]);
  };

  const handleNextSkip = (currRow) => {
    sound.stop();
    currRow + 1 < props.tracks.length
      ? handlePlayClick(currRow + 1, props.tracks[currRow + 1])
      : handlePlayClick(currRow, props.tracks[currRow]);
  };

  const convertTrackDuration = (duration) => {
    const minutes = convert(duration).minutes;
    const seconds = convert(duration).seconds;

    return seconds >= 0 && seconds <= 9
      ? `${minutes}:0${seconds}`
      : `${minutes}:${seconds}`;
  };

  const convertLongTitles = (title) => {
    return title.length > 20 ? `${title.substring(0, 20)}...` : title;
  };

  const displayTableBody = () => {
    return (
      <TableBody>
        {props.tracks.map((track, rowIndex) => (
          <TableRow
            hover
            key={track.title}
            selected={rowIndex === selectedRow}
            classes={{ hover: classes.hover, selected: classes.selected }}
          >
            <TableCell align="center" className={classes.tableCell}>
              <IconButton
                color="inherit"
                style={{ padding: 0 }}
                disabled={rowIndex === selectedRow && disablePlayButton}
                onClick={() => {
                  if (rowIndex !== selectedRow && sound) {
                    childRef.current.resetTrackSeek();
                  }
                  handlePlayClick(rowIndex, track);
                }}
              >
                {rowIndex === selectedRow ? (
                  isTrackPlaying ? (
                    <PauseCircleOutlineRoundedIcon></PauseCircleOutlineRoundedIcon>
                  ) : (
                    <PlayCircleFilledWhiteOutlinedIcon></PlayCircleFilledWhiteOutlinedIcon>
                  )
                ) : (
                  <PlayCircleFilledWhiteOutlinedIcon></PlayCircleFilledWhiteOutlinedIcon>
                )}
              </IconButton>
            </TableCell>
            <TableCell className={classes.tableCell}>
              <Tooltip
                TransitionComponent={Zoom}
                arrow
                enterDelay={600}
                title={track.title}
              >
                <Link color="inherit" underline="none">
                  {convertLongTitles(track.title)}
                </Link>
              </Tooltip>
            </TableCell>
            <TableCell className={classes.tableCell}>
              {track.genre.name}
            </TableCell>
            <TableCell className={classes.tableCell}>{track.rank}</TableCell>
            <TableCell className={classes.tableCell}>
              <Tooltip
                TransitionComponent={Zoom}
                arrow
                enterDelay={600}
                title={track.album.title}
              >
                <Link color="inherit" underline="none">
                  {convertLongTitles(track.album.title)}
                </Link>
              </Tooltip>
            </TableCell>
            <TableCell className={classes.tableCell}>
              <Tooltip
                TransitionComponent={Zoom}
                arrow
                enterDelay={600}
                title={track.artist.name}
              >
                <Link color="inherit" underline="none">
                  {convertLongTitles(track.artist.name)}
                </Link>
              </Tooltip>
            </TableCell>
            <TableCell className={classes.tableCell}>
              {convertTrackDuration(track.duration)}
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    );
  };

  const displayLoadingTableBody = () => {
    return (
      <TableBody>
        {[...Array(9)].map((x, i) => (
          <TableRow hover key={i}>
            <TableCell align="center">
              <Skeleton
                variant="circle"
                animation="wave"
                width={28}
                height={23}
              />
            </TableCell>
            <TableCell>
              <Skeleton animation="wave" style={{ width: 130 }} />
            </TableCell>
            <TableCell>
              <Skeleton animation="wave" style={{ width: 60 }} />
            </TableCell>
            <TableCell>
              <Skeleton animation="wave" style={{ width: 60 }} />
            </TableCell>
            <TableCell>
              <Skeleton animation="wave" style={{ width: 140 }} />
            </TableCell>
            <TableCell>
              <Skeleton animation="wave" style={{ width: 150 }} />
            </TableCell>
            <TableCell>
              <Skeleton animation="wave" style={{ width: 40 }} />
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    );
  };

  return (
    <div>
      {selectedTrack && (
        <TrackPlayer
          ref={childRef}
          skipPrevSong={() => {
            handlePrevSkip(selectedRow);
          }}
          pauseSong={() =>
            handlePlayClick(selectedRow, props.tracks[selectedRow])
          }
          skipNextSong={() => {
            handleNextSkip(selectedRow);
          }}
          changeTrackVolume={handleVolumeChange}
          changeTrackSeek={handleTrackSeek}
          currentTrack={selectedTrack}
          isTrackPlaying={isTrackPlaying}
          disablePlay={disablePlayButton}
          seek={sound ? sound.seek() : 0}
        ></TrackPlayer>
      )}
      <div style={{ textAlign: "center" }}>
        <Box display="inline-block" className={classes.tableBox}>
          <TableContainer className={classes.table}>
            <Table stickyHeader>
              <TableHead>
                <TableRow>
                  <TableCell className={classes.tableHeaderCell}></TableCell>
                  <TableCell className={classes.tableHeaderCell}>
                    TITLE
                  </TableCell>
                  <TableCell className={classes.tableHeaderCell}>
                    GENRE
                  </TableCell>
                  <TableCell className={classes.tableHeaderCell}>
                    RANK
                  </TableCell>
                  <TableCell className={classes.tableHeaderCell}>
                    ALBUM
                  </TableCell>
                  <TableCell className={classes.tableHeaderCell}>
                    ARTIST
                  </TableCell>
                  <TableCell className={classes.tableHeaderCell}>
                    <QueryBuilderRoundedIcon></QueryBuilderRoundedIcon>
                  </TableCell>
                </TableRow>
              </TableHead>
              {props.tracks ? displayTableBody() : displayLoadingTableBody()}
            </Table>
          </TableContainer>
        </Box>
      </div>
    </div>
  );
};

const useStyles = makeStyles({
  tableBox: {
    marginBottom: 25,
    width: 1200,
  },
  table: {
    maxHeight: 333,
    width: 1050,
    display: "inline-flex",
  },
  tableHeaderCell: {
    backgroundColor: "#1c1c1c",
    color: "#757575",
    padding: 6,
    fontSize: 13,
    letterSpacing: 1.5,
    borderColor: "#555555",
  },
  tableCell: {
    fontSize: 14,
    color: "whitesmoke",
    padding: 10,
    borderColor: "#555555",
    "$selected &": {
      color: "#C70039",
    },
  },
  hover: {
    "&$hover:hover": {
      backgroundColor: "#444444",
    },
  },
  selected: {
    "&$selected": {
      backgroundColor: "#212121",
    },
    "&$selected:hover": {
      backgroundColor: "#444444",
    },
  },
});

export default TracksTable;
