import React from "react";
import cx from "clsx";
import { makeStyles } from "@material-ui/styles";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import ButtonBase from "@material-ui/core/ButtonBase";
import TextInfoCardContent from "@mui-treasury/components/cardContent/textInfo";
import { useBlogCardContentStyles } from "@mui-treasury/styles/cardContent/blog";
import Skeleton from "@material-ui/lab/Skeleton";
import convert from "convert-seconds";

const PlaylistInfo = props => {
  const classes = useStyles();
  const {
    button: buttonStyles,
    ...cardContentStyles
  } = useBlogCardContentStyles();

  const convertTravelDuration = duration => {
    const hours = convert(duration).hours;
    const minutes = convert(duration).minutes;
    const seconds = convert(duration).seconds;

    if (hours === 0) {
      if (minutes === 0) {
        return `${seconds}s`;
      }
      return seconds === 0 ? `${minutes}m` : `${minutes}m ${seconds}s`;
    }
    return minutes === 0 ? `${hours}h` : `${hours}h ${minutes}m`;
  };

  const displayPlaylistDescription = playlist => {
    let songWriters = "";
    for (let i = 0; i < 4; i++) {
      songWriters += `${playlist.tracks[i].artist.name}, `;
    }
    return `${playlist.tracks.length} songs, With ${songWriters} and more`;
  };

  const isPlaylistLoaded = () => {
    return (
      props.playlist.tracks &&
      props.playlist.coverImage &&
      props.playlist.totalDuration &&
      props.playlist.avgRank &&
      props.playlist.title
    );
  };

  return isPlaylistLoaded() ? (
    <Card className={classes.root}>
      <CardMedia
        className={classes.media}
        image={`data:image/png;base64, ${props.playlist.coverImage}`}
      />
      <CardContent>
        <TextInfoCardContent
          classes={cardContentStyles}
          overline={
            "Total Duration: " +
            convertTravelDuration(props.playlist.totalDuration)
          }
          heading={props.playlist.title}
          body={displayPlaylistDescription(props.playlist)}
        />
        <ButtonBase
          disabled={true}
          className={cx(classes.rankButton, buttonStyles)}
        >
          <span style={{ color: "whitesmoke", letterSpacing: 1 }}>
            {`Rank ${props.playlist.avgRank}`}
          </span>
        </ButtonBase>
      </CardContent>
    </Card>
  ) : (
    <Card className={classes.root}>
      <CardMedia className={classes.media} src={"/"} />
      <CardContent>
        <Skeleton animation="wave" style={{ width: 200, marginBottom: 20 }} />
        <Skeleton animation="wave" style={{ width: 300, marginBottom: 10 }} />
        <Skeleton animation="wave" style={{ width: 600, marginBottom: 22 }} />
        <Skeleton animation="wave" style={{ width: 200, padding: 14 }} />
      </CardContent>
    </Card>
  );
};

const useStyles = makeStyles(({ breakpoints }) => ({
  root: {
    margin: "auto",
    borderRadius: 16,
    boxShadow: "0px 14px 70px rgba(34, 35, 58, 0.2)",
    maxWidth: "75%",
    maxHeight: 210,
    overflow: "initial",
    background: "#202020",
    display: "flex",
    flexDirection: "column",
    marginTop: "20px",
    marginBottom: "35px",
    alignItems: "center",
    [breakpoints.up("md")]: {
      flexDirection: "row"
    }
  },
  media: {
    paddingBottom: "40%",
    [breakpoints.up("md")]: {
      width: "35%",
      marginLeft: -26,
      transform: "translateX(-8px)",
      backgroundSize: "contain",
      borderRadius: 10
    }
  },
  rankButton: {
    padding: 10,
    width: 200,
    marginTop: 25
  }
}));

export default PlaylistInfo;
