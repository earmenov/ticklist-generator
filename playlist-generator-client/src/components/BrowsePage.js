import React, { useState } from "react";
import PlaylistDisplayer from "./PlaylistDisplayer";
import Navbar from "./Navbar";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import styled from "@emotion/styled/macro";

const BrowsePage = () => {
  const [userSearch, setUserSearch] = useState("");

  const handleUserInputChange = (event) => {
    setUserSearch(event.target.value);
  };

  return (
    <div>
      <Navbar></Navbar>
      <Header>ALL PLAYLISTS</Header>
      <Divider style={{ height: 2, margin: "0 9% 4% 9%" }}></Divider>
      <TextField
        placeholder="Search User..."
        color="secondary"
        onChange={handleUserInputChange}
        style={{ left: "41%", width: "18%" }}
      ></TextField>
      <PlaylistDisplayer
        showUser={true}
        username={userSearch}
        isEditable={false}
      ></PlaylistDisplayer>
    </div>
  );
};

const Header = styled.h1({
  textAlign: "center",
  color: "#38393b",
  margin: "8% 0 1% 0",
  letterSpacing: 1,
});

export default BrowsePage;
