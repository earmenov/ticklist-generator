import React, { useState } from "react";
import "../../css/SignUpInForm.css";
import SignUpForm from "./SignUpForm";
import SignInForm from "./SignInForm";

const SignUpInForm = () => {
  const [container, setContainer] = useState("container");

  return (
    <div className="bodyLoginForm">
      <div className={container} id="container">
        <SignUpForm onSignUpClick={value => setContainer(value)}></SignUpForm>
        <SignInForm></SignInForm>
        <div className="overlay-container">
          <div className="overlay">
            <div className="overlay-panel overlay-left">
              <h1>Welcome Back!</h1>
              <p>
                To keep connected with us please login with your personal info
              </p>
              <button
                className="buttonLoginForm ghost"
                id="signIn"
                onClick={() => setContainer("container")}
              >
                Sign In
              </button>
            </div>
            <div className="overlay-panel overlay-right">
              <h1>Hello, Friend!</h1>
              <p>Enter your personal details and start journey with us</p>
              <button
                className="buttonLoginForm ghost"
                id="signUp"
                onClick={() => setContainer("container right-panel-active")}
              >
                Sign Up
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignUpInForm;
