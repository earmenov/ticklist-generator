import React, { useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import "../../css/SignUpInForm.css";
import { TextField } from "@material-ui/core";
import cookie from "js-cookie";
import { useDispatch } from "react-redux";
import { VALID_EMAIL_PATTERN } from "../../constants";

const setLogin = user => ({ type: "SET_LOGIN", payload: user });

const SignInForm = () => {
  const [signInValues, setSignInValues] = useState({ email: "", password: "" });
  const [signInErrors, setSignInErrors] = useState({});
  const [badCredentials, setBadCredentials] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();

  const styleTextField = {
    marginBottom: "16px",
    width: "250px",
    marginTop: 5
  };

  const validateSignIn = values => {
    let errors = {};
    const validEmailRegex = RegExp(VALID_EMAIL_PATTERN);

    if (!values.email) {
      errors.email = "This field is required";
    } else if (!validEmailRegex.test(values.email)) {
      errors.email = "Email address is invalid";
    }
    if (!values.password) {
      errors.password = "This field is required";
    }

    return errors;
  };

  const handleSignInChange = event => {
    const { name, value } = event.target;
    setSignInValues({
      ...signInValues,
      [name]: value
    });
  };

  const submitSignIn = event => {
    event.preventDefault();
    const errors = validateSignIn(signInValues);
    setSignInErrors(errors);

    if (Object.keys(errors).length === 0) {
      const loginRequest = {
        usernameOrEmail: signInValues.email,
        password: signInValues.password
      };

      axios
        .post(`/api/auth/signin`, loginRequest)
        .then(res => {
          cookie.set("token", res.data.accessToken);
          setBadCredentials(false);
        })
        .then(() => {
          axios.defaults.headers.common["Authorization"] = `Bearer ${cookie.get(
            "token"
          )}`;
          axios
            .get(`/api/user/me`)
            .then(res => {
              dispatch(setLogin(res.data));
              console.log(res.data.authorities[0].authority);
            })
            .catch(() => {
              cookie.remove("token");
              window.location.reload();
            });
        })
        .then(() => {
          history.push("/home");
        })
        .catch(() => {
          setBadCredentials(true);
        });
    }
  };

  return (
    <div className="loginFormContainer sign-in-container">
      <form className="loginForm" action="#" onSubmit={submitSignIn} noValidate>
        <h1>Sign in</h1>
        <div className="social-container"></div>
        <span style={{ marginBottom: "15px" }}>or use your account</span>
        <TextField
          style={styleTextField}
          error={signInErrors.email ? true : false}
          name="email"
          type="email"
          placeholder="Email"
          value={signInValues.email}
          onChange={handleSignInChange}
        />
        {signInErrors.email && (
          <p id="inputErrorMessage">{signInErrors.email}</p>
        )}
        <TextField
          style={styleTextField}
          error={signInErrors.password ? true : false}
          name="password"
          type="password"
          placeholder="Password"
          value={signInValues.password}
          onChange={handleSignInChange}
        />
        {signInErrors.password && (
          <p id="inputErrorMessage">{signInErrors.password}</p>
        )}
        {/* <a className="loginFormLink" href="#">
              Forgot your password?
            </a> */}
        <button className="buttonLoginForm">Sign In</button>
        {badCredentials && (
          <p id="badCredentials">Bad credentials, please try again</p>
        )}
      </form>
    </div>
  );
};

export default SignInForm;
