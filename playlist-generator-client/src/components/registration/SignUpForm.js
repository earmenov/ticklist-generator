import React, { useState } from "react";
import axios from "axios";
import "../../css/SignUpInForm.css";
import { TextField } from "@material-ui/core";
import Swal from "sweetalert2";
import styled from "@emotion/styled/macro";
import { VALID_EMAIL_PATTERN } from "../../constants";

const SignUpForm = (props) => {
  const [signUpValues, setSignUpValues] = useState({
    username: "",
    email: "",
    password: "",
    confPassword: "",
  });
  const [signUpErrors, setSignUpErrors] = useState({});
  const [signUpServerErrorMsg, setSignUpServerErrorMsg] = useState("");

  const styleTextField = {
    marginBottom: "16px",
    width: "250px",
    marginTop: 8,
  };

  const validateSignUp = (values) => {
    let errors = {};
    const validEmailRegex = RegExp(VALID_EMAIL_PATTERN);

    if (!values.username) {
      errors.username = "This field is required";
    } else if (values.username.length < 5) {
      errors.username = "Username needs to be more than 5 characters";
    }

    if (!values.email) {
      errors.email = "This field is required";
    } else if (!validEmailRegex.test(values.email)) {
      errors.email = "Email address is invalid";
    }

    if (!values.password) {
      errors.password = "This field is required";
    } else if (values.password.length < 8) {
      errors.password = "Password needs to be more than 8 characters";
    }

    if (!values.confPassword) {
      errors.confPassword = "This field is required";
    } else if (values.confPassword !== values.password) {
      errors.confPassword = "Passwords does not match";
    }

    return errors;
  };

  const handleSignUpChange = (event) => {
    const { name, value } = event.target;
    setSignUpValues({
      ...signUpValues,
      [name]: value,
    });
  };

  const submitSignUp = (event) => {
    event.preventDefault();
    const errors = validateSignUp(signUpValues);
    setSignUpErrors(errors);

    if (Object.keys(errors).length === 0) {
      const signUpRequest = {
        username: signUpValues.username,
        email: signUpValues.email,
        password: signUpValues.password,
      };

      axios
        .post(`/api/auth/signup`, signUpRequest)
        .then((response) => {
          props.onSignUpClick("container");
          setSignUpServerErrorMsg("");
          signUpValues.username = "";
          signUpValues.email = "";
          signUpValues.password = "";
          signUpValues.confPassword = "";
          return response.data.message;
        })
        .then((successMessage) => {
          Swal.fire({
            title: "Registration Successful",
            type: "success",
            text: successMessage,
            timer: "1300",
          });
        })
        .catch((e) => {
          setSignUpServerErrorMsg(e.response.data.message);
        });
    }
  };

  return (
    <div className="loginFormContainer sign-up-container">
      <form className="loginForm" action="#" onSubmit={submitSignUp} noValidate>
        <h1>Create Account</h1>
        <div className="social-container"></div>
        <span style={{ marginBottom: "15px" }}>
          or use your email for registration
        </span>
        <TextField
          color="secondary"
          style={styleTextField}
          error={Boolean(signUpErrors.username)}
          name="username"
          type="text"
          placeholder="Username"
          inputProps={{
            maxLength: 20,
          }}
          value={signUpValues.username || ""}
          onChange={handleSignUpChange}
        />
        {signUpErrors.username && (
          <p id="inputErrorMessage">{signUpErrors.username}</p>
        )}
        <TextField
          style={styleTextField}
          error={Boolean(signUpErrors.email)}
          name="email"
          type="email"
          placeholder="Email"
          inputProps={{
            maxLength: 60,
          }}
          value={signUpValues.email}
          onChange={handleSignUpChange}
        />
        {signUpErrors.email && (
          <p id="inputErrorMessage">{signUpErrors.email}</p>
        )}
        <TextField
          style={styleTextField}
          error={Boolean(signUpErrors.password)}
          name="password"
          type="password"
          placeholder="Password"
          inputProps={{
            maxLength: 127,
          }}
          value={signUpValues.password}
          onChange={handleSignUpChange}
        />
        {signUpErrors.password && (
          <p id="inputErrorMessage">{signUpErrors.password}</p>
        )}
        <TextField
          style={styleTextField}
          error={Boolean(signUpErrors.confPassword)}
          name="confPassword"
          type="password"
          placeholder="Confirm Password"
          inputProps={{
            maxLength: 127,
          }}
          value={signUpValues.confPassword}
          onChange={handleSignUpChange}
        />
        {signUpErrors.confPassword && (
          <p id="inputErrorMessage">{signUpErrors.confPassword}</p>
        )}
        <button className="buttonLoginForm">Sign Up</button>
        {signUpServerErrorMsg !== "" && (
          <ServerError>{signUpServerErrorMsg}</ServerError>
        )}
      </form>
    </div>
  );
};

const ServerError = styled.p({
  color: "red",
  marginBottom: 18,
  fontWeight: 500,
});

export default SignUpForm;
