import React, { useState } from "react";
import { PROGRESS_BAR_MAX_VAL } from "../constants";
import {
  TextField,
  Input,
  Tooltip,
  Zoom,
  Button,
  InputAdornment,
  Link,
} from "@material-ui/core";
import Swal from "sweetalert2";
import styled from "@emotion/styled/macro";
import axios from "axios";

const PlaylistEditForm = React.forwardRef((props, ref) => {
  ref = { ref };

  const [editPreferences, setEditPreferences] = useState(false);
  const [titleEdit, setTitleEdit] = useState(props.playlist.title);
  const [progressValue, setProgressValue] = useState(0);
  const [image, setImage] = useState(null);
  const [imagePreview, setImagePreview] = useState(
    `data:image/png;base64, ${props.playlist.coverImage}`
  );
  const [genrePreferences, setGenrePreferences] = useState(
    new Array(props.playlist.genres.length).fill(0)
  );
  const [serverErrors, setServerErrors] = useState([]);

  const handleInputTitleChange = (event) => {
    setTitleEdit(event.target.value);
  };

  const handleImageChange = (file) => {
    setImagePreview(URL.createObjectURL(file));
    setImage(file);
  };

  const createFormData = (object) => {
    const formData = new FormData();
    Object.keys(object).forEach((key) => formData.append(key, object[key]));
    return formData;
  };

  const createEditCriteria = () => {
    const playlistEditCriteria = image
      ? {
          id: props.playlist.id,
          title: titleEdit,
          image: image,
          jsonGenrePreferences: editPreferences
            ? JSON.stringify(genrePreferences)
            : null,
        }
      : {
          id: props.playlist.id,
          title: titleEdit,
          jsonGenrePreferences: editPreferences
            ? JSON.stringify(genrePreferences)
            : null,
        };
    return createFormData(playlistEditCriteria);
  };

  const handlePlaylistEdit = () => {
    axios
      .put(`/api/playlist/edit`, createEditCriteria())
      .then((newPlaylist) => {
        setServerErrors([]);
        props.closeModal(newPlaylist.data);
      })
      .then(() => {
        Swal.fire({
          title: "Playlist Edit Successful",
          type: "success",
          timer: "1500",
        });
      })
      .catch((e) => {
        setServerErrors(e.response.data.errors);
      });
  };

  const handlePercentageChange = (value, index) => {
    const list = genrePreferences;
    list[index] = Number(value);
    setGenrePreferences(list);

    const newProgressSum = genrePreferences.reduce(
      (result, preference) => result + preference
    );

    if (newProgressSum <= PROGRESS_BAR_MAX_VAL) {
      setProgressValue(newProgressSum);
    }
  };

  const renderGenres = () => {
    return [...props.playlist.genres].map((genre, index) => (
      <div key={genre.id}>
        <label id="genreLabel">{genre.name}</label>
        <Input
          id="percentInput"
          color="secondary"
          inputProps={{
            maxLength: 2,
          }}
          placeholder={"0"}
          onChange={(event) =>
            handlePercentageChange(event.target.value, index)
          }
          endAdornment={
            <InputAdornment className="percentIcon" position="end">
              %
            </InputAdornment>
          }
        />
      </div>
    ));
  };

  const renderGenreEdit = () => {
    return (
      <div>
        {renderGenres()}
        <progress
          max={PROGRESS_BAR_MAX_VAL}
          value={progressValue}
          data-label={progressValue + " %"}
        ></progress>
      </div>
    );
  };

  const isEditDisabled = () => {
    return (
      titleEdit === "" ||
      (editPreferences && progressValue !== PROGRESS_BAR_MAX_VAL)
    );
  };

  return (
    <form className="editBox">
      <TextField
        label="Playlist Title"
        variant="filled"
        color="secondary"
        style={{ width: "75%" }}
        defaultValue={titleEdit}
        onChange={handleInputTitleChange}
      ></TextField>
      <div style={{ marginBottom: "4%" }}></div>
      {editPreferences && renderGenreEdit()}
      <div style={{ display: "inline" }}>
        <Button
          id="editPlaylistButton"
          variant="contained"
          color="secondary"
          component="label"
        >
          Playlist Cover
          <Input
            type="file"
            inputProps={{ accept: "image/*" }}
            style={{ display: "none" }}
            onChange={(event) => handleImageChange(event.target.files[0])}
          />
        </Button>
      </div>
      <div
        style={{ width: "14%", marginTop: "5%", display: "inline-block" }}
      ></div>
      <img id="playlistEditImg" alt="" src={imagePreview} />
      <div>
        <Button
          id="editPlaylistButton"
          variant="contained"
          color="secondary"
          onClick={() => {
            setTitleEdit(props.playlist.title);
            props.closeModal();
          }}
        >
          Close
        </Button>
        <div
          style={{ width: "16%", marginTop: "8%", display: "inline-block" }}
        ></div>
        <Tooltip
          TransitionComponent={Zoom}
          arrow
          placement="top"
          TransitionProps={{ timeout: 200 }}
          title={""}
        >
          <span>
            <Button
              id="editPlaylistButton"
              variant="contained"
              disabled={isEditDisabled()}
              color="secondary"
              onClick={handlePlaylistEdit}
            >
              Edit Playlist
            </Button>
          </span>
        </Tooltip>
      </div>
      {editPreferences ? (
        <div style={{ margin: "3%" }}>
          <Link
            style={{ display: "block", fontSize: 17 }}
            href="#"
            onClick={() => {
              setEditPreferences(false);
            }}
          >
            cancel preferences edit
          </Link>
        </div>
      ) : (
        <div style={{ margin: "3%" }}>
          <Link
            style={{ display: "block", fontSize: 17 }}
            href="#"
            onClick={() => {
              setEditPreferences(true);
            }}
          >
            change genre preferences
          </Link>
        </div>
      )}
      {serverErrors &&
        serverErrors.map((error, index) => (
          <ServerError key={index}>{error}</ServerError>
        ))}
    </form>
  );
});

const ServerError = styled.p({
  color: "red",
  marginBottom: 18,
  fontWeight: 500,
});

export default PlaylistEditForm;
