import React, { useState } from "react";
import Playlist from "./track-player/Playlist";
import PlaylistEditForm from "./PlaylistEditForm";
import cx from "clsx";
import { makeStyles } from "@material-ui/styles";
import Modal from "@material-ui/core/Modal";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import EditRoundedIcon from "@material-ui/icons/EditRounded";
import DeleteRoundedIcon from "@material-ui/icons/DeleteRounded";
import IconButton from "@material-ui/core/IconButton";
import CardActionArea from "@material-ui/core/CardActionArea";
import TextInfoCardContent from "@mui-treasury/components/cardContent/textInfo";
import { useFourThreeCardMediaStyles } from "@mui-treasury/styles/cardMedia/fourThree";
import { useText04CardContentStyles } from "@mui-treasury/styles/cardContent/text04";
import { useBouncyShadowStyles } from "@mui-treasury/styles/shadow/bouncy";
import styled from "@emotion/styled/macro";
import convert from "convert-seconds";
import axios from "axios";

const PlaylistCard = (props) => {
  const styles = useStyles();
  const mediaStyles = useFourThreeCardMediaStyles();
  const textCardContentStyles = useText04CardContentStyles();
  const shadowStyles = useBouncyShadowStyles();

  const [modalOpen, setModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [playlist, setPlaylist] = useState(props.playlist);

  const convertTravelDuration = (duration) => {
    const hours = convert(duration).hours;
    const minutes = convert(duration).minutes;
    const seconds = convert(duration).seconds;

    if (hours === 0) {
      if (minutes === 0) {
        return `${seconds}s`;
      }
      return seconds === 0 ? `${minutes}m` : `${minutes}m ${seconds}s`;
    }
    return minutes === 0 ? `${hours}h` : `${hours}h ${minutes}m`;
  };

  const handleModalOpen = () => {
    setModalOpen(true);
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  const handleEditModalOpen = (event) => {
    event.stopPropagation();
    setEditModalOpen(true);
  };

  const handleEditModalClose = (newPlaylist) => {
    setPlaylist(newPlaylist);
    setEditModalOpen(false);
  };

  const handleDeleteButton = (event) => {
    event.stopPropagation();
    axios.delete(`/api/playlist/delete/${playlist.id}`).then(() => {
      window.location.reload();
    });
  };

  return (
    <CardActionArea style={{ color: "white", borderRadius: "14px" }}>
      <Card
        className={cx(styles.root, shadowStyles.root)}
        onClick={handleModalOpen}
      >
        <Background>
          <DisplayOver>
            <Hover>
              <Rank>
                <div style={{ display: "inline-flex" }}>
                  {props.isEditable && (
                    <IconButton
                      style={{ marginBottom: 6 }}
                      onClick={handleEditModalOpen}
                    >
                      <EditRoundedIcon />
                    </IconButton>
                  )}
                  <h3
                    style={{ margin: 14 }}
                  >{`Rank ${playlist.avgRank}`}</h3>
                  {props.isEditable && (
                    <IconButton
                      style={{ marginBottom: 6 }}
                      onClick={handleDeleteButton}
                    >
                      <DeleteRoundedIcon />
                    </IconButton>
                  )}
                </div>
              </Rank>
            </Hover>
            <CardMedia
              className={cx(styles.media, mediaStyles.root)}
              image={`data:image/png;base64, ${playlist.coverImage}`}
            />
            <CardContent className={styles.content}>
              <TextInfoCardContent
                classes={textCardContentStyles}
                overline={
                  "Total Duration: " +
                  convertTravelDuration(playlist.totalDuration)
                }
                heading={playlist.title}
              />
            </CardContent>
          </DisplayOver>
        </Background>
      </Card>
      <Modal
        className={styles.modal}
        open={modalOpen}
        onClose={handleModalClose}
        closeAfterTransition
      >
        <Playlist playlistId={playlist.id}></Playlist>
      </Modal>
      <Modal
        className={styles.modal}
        open={editModalOpen}
        onClose={handleEditModalClose}
        closeAfterTransition
      >
        <PlaylistEditForm
          playlist={playlist}
          closeModal={handleEditModalClose}
        ></PlaylistEditForm>
      </Modal>
    </CardActionArea>
  );
};

const useStyles = makeStyles(() => ({
  root: {
    width: 270,
    backgroundColor: "#212121",
    borderRadius: 12,
  },
  media: {
    borderRadius: 6,
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
}));

const Hover = styled.div({
  opacity: 0,
  textAlign: "center",
  transition: "opacity 350ms ease",
});

const DisplayOver = styled.div({
  textAlign: "center",
  transition: "background-color 350ms ease",
  backgroundColor: "transparent",
  padding: "20px 20px 0 20px",
});

const Rank = styled.h4({
  transform: "translate3d(0,43px,0)",
  transition: "transform 500ms ease",
  position: "absolute",
  minInlineSize: "-webkit-fill-available",
  right: "0",
  opacity: "0",
  color: "#808080",
  fontFamily: "Helvetica",
  fontSize: "16px",
});

const Background = styled.div({
  [`:hover ${DisplayOver}`]: {
    backgroundColor: "#F0F0F0",
  },
  [`:hover ${Rank}`]: {
    transform: "translate3d(0, 27px, 0)",
    opacity: 1,
    bottom: 1,
  },
  [`:hover ${Hover}`]: {
    opacity: 1,
  },
});

export default PlaylistCard;
