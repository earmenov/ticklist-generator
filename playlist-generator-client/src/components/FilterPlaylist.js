import React, { useState, useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import SearchRoundedIcon from "@material-ui/icons/SearchRounded";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
import SpeedDial from "@material-ui/lab/SpeedDial";
import Button from "@material-ui/core/Button";
import Slider from "@material-ui/core/Slider";
import Select from "@material-ui/core/Select";
import FormControl from "@material-ui/core/FormControl";
import ImportExportRoundedIcon from "@material-ui/icons/ImportExportRounded";
import Tooltip from "@material-ui/core/Tooltip";
import Zoom from "@material-ui/core/Zoom";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";
import Chip from "@material-ui/core/Chip";
import axios from "axios";
import styled from "@emotion/styled/macro";

const FilterPlaylist = (props) => {
  const classes = useStyles();

  const [openFilter, setOpenFilter] = useState(false);
  const [sortAsc, setSortAsc] = useState(true);
  const [sortValue, setSortValue] = useState("");
  const [playlistTitle, setPlaylistTitle] = useState("");
  const [durationValue, setDurationValue] = useState([0, 24]);
  const [playlistGenres, setPlaylistGenres] = useState([]);
  const [selectedGenres, setSelectedGenres] = useState([]);

  useEffect(() => {
    axios
      .get(`/api/genres/get-all`)
      .then((fetchedGenres) => {
        setPlaylistGenres(fetchedGenres.data);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  const handleGenreSelect = (event) => {
    setSelectedGenres(event.target.value);
  };

  const handleDurationChange = (event, newValue) => {
    setDurationValue(newValue);
  };

  const handleTitleChange = (event) => {
    setPlaylistTitle(event.target.value);
  };

  const handleSearchClick = () => {
    const searchCriteria = {
      playlistTitle: playlistTitle,
      fromDuration: durationValue[0],
      toDuration: durationValue[1],
      sortBy: sortValue,
      asc: sortAsc,
      checkedGenres: selectedGenres.join(","),
    };

    props.onSearchSelect(searchCriteria);
    setOpenFilter(false);
  };

  const handleClearClick = () => {
    setSortAsc(true);
    setSortValue("");
    setPlaylistTitle("");
    setDurationValue([0, 24]);
    setSelectedGenres([]);
  };

  const filterField = () => (
    <div>
      <FilterLabel
        style={{
          marginTop: 15,
        }}
      >
        Filter by
      </FilterLabel>
      <TextField
        label="Playlist Title"
        variant="outlined"
        className={classes.titleFilter}
        onChange={handleTitleChange}
        value={playlistTitle}
      />
      <TrackDurationFilter
        className={classes.slier}
        value={durationValue}
        min={0}
        max={24}
        onChange={handleDurationChange}
        valueLabelDisplay="on"
        aria-labelledby="range-slider"
        valueLabelFormat={(n, index) => {
          return `${durationValue[index]}h`;
        }}
      />
      <FilterLabel
        style={{
          marginBottom: 12,
        }}
      >
        Playlist Duration
      </FilterLabel>

      {playlistGenres && (
        <div>
          <FormControl style={{ margin: 15, width: 260 }}>
            <Select
              labelId="mutiple-checkbox-label"
              id="mutiple-checkbox"
              multiple
              value={selectedGenres}
              onChange={handleGenreSelect}
              input={<Input />}
              renderValue={(selected) => (
                <div className={classes.chips}>
                  {selected.map((value) => (
                    <Chip key={value} label={value} style={{ margin: 2 }} />
                  ))}
                </div>
              )}
            >
              {playlistGenres.map((genre) => (
                <MenuItem key={genre.id} value={genre.name}>
                  <Checkbox checked={selectedGenres.indexOf(genre.name) > -1} />
                  <ListItemText primary={genre.name} />
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <FilterLabel>Filter By Genres</FilterLabel>
        </div>
      )}
    </div>
  );

  const sortField = () => (
    <div>
      <FilterLabel
        style={{
          marginTop: 15,
        }}
      >
        Sort by
      </FilterLabel>
      <FormControl style={{ margin: 15, width: 220 }}>
        <Select
          variant="outlined"
          value={sortValue}
          onChange={(e) => {
            setSortValue(e.target.value);
          }}
        >
          <MenuItem value={""}>None</MenuItem>
          <MenuItem value={"title"}>Title</MenuItem>
          <MenuItem value={"avgRank"}>Rank</MenuItem>
          <MenuItem value={"totalDuration"}>Duration</MenuItem>
        </Select>
      </FormControl>
      <Tooltip
        placement="top"
        arrow
        title={sortAsc ? "Sort Ascending" : "Sort Descending"}
        TransitionComponent={Zoom}
      >
        <IconButton
          style={{
            borderLeft: "dashed",
            borderRight: "dashed",
            margin: "18px -9px 0px 30px",
            color: sortAsc ? "#108a03" : "#c40606",
            display: sortValue === "" && "none",
          }}
          size="medium"
          onClick={() => {
            setSortAsc(!sortAsc);
          }}
        >
          <ImportExportRoundedIcon />
        </IconButton>
      </Tooltip>
    </div>
  );

  return (
    <div>
      <Drawer
        open={openFilter}
        onClose={() => {
          setOpenFilter(false);
        }}
        style={{ textAlign: "center" }}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Header>
          <FilterLabel>Filter and Sort options</FilterLabel>
        </Header>
        <Divider />
        {sortField()}
        <Divider />
        {filterField()}
        <Divider style={{ marginTop: 20 }} />
        <div style={{ marginTop: "3%" }}>
          <Button
            variant="contained"
            className={classes.filterFormButton}
            onClick={handleClearClick}
          >
            Clear
          </Button>
          <Button
            variant="contained"
            color="secondary"
            className={classes.filterFormButton}
            onClick={handleSearchClick}
          >
            Search
          </Button>
        </div>
      </Drawer>
      <SpeedDial
        ariaLabel="SpeedDial"
        style={{ position: "fixed", bottom: "3%", right: "3%" }}
        hidden={openFilter}
        open={true}
        icon={<SearchRoundedIcon />}
        onClick={() => {
          setOpenFilter(true);
        }}
      ></SpeedDial>
    </div>
  );
};

const Header = styled.div({
  padding: "4%",
});

const FilterLabel = styled.div({
  display: "flex",
  justifyContent: "center",
  fontWeight: "700",
  fontVariantCaps: "all-petite-caps",
  letterSpacing: 1,
});

const TrackDurationFilter = withStyles({
  root: {
    maxWidth: "75%",
    marginTop: 32,
  },
  track: {
    height: 4,
  },
  rail: {
    height: 4,
    borderRadius: 4,
  },
})(Slider);

const useStyles = makeStyles(() => ({
  titleFilter: {
    width: 300,
    margin: 20,
  },
  chips: {
    display: "flex",
    flexWrap: "wrap",
  },
  filterFormButton: {
    margin: 15,
    width: 100,
    letterSpacing: 1,
    fontSize: 13,
    padding: "9px 0 9px 0",
  },
  drawerPaper: {
    width: "25%",
  },
}));

export default FilterPlaylist;
