import React from "react";
import "../../css/PlaylistGeneratorForm.css";
import "../../css/ProgressBar.css";
import Navbar from "../Navbar";
import DefaultPlaylistPic from "../../default-playlist-img.jpg";
import * as reducer from "../../store/reducers/generatorReducer";
import SecondForm from "./SecondForm";
import FirstForm from "./FirstForm";
export const AppContext = React.createContext();

const PlaylistGeneratorForm = () => {
  const [state, dispatch] = React.useReducer(reducer.generatorReducer, {
    genres: [],
    nextStep: false,
    progressValue: 0,
    checkedGenres: new Map(),
    buttonNextDisabled: true,
    buttonGenerateDisabled: true,
    playlistTitle: "",
    originAddress: "",
    destinationAddress: "",
    originCoords: {},
    destinationCoords: {},
    imagePreview: DefaultPlaylistPic,
    image: null,
    topTracks: false,
    sameArtist: false,
    errors: {
      originAddressError: "",
      destinationAddressError: "",
    },
  });

  return (
    <div>
      <Navbar></Navbar>
      {state.nextStep ? (
        <AppContext.Provider value={{ state, dispatch }}>
          <SecondForm stateSecond={state}></SecondForm>
        </AppContext.Provider>
      ) : (
        <AppContext.Provider value={{ state, dispatch }}>
          <FirstForm stateFirst={state}></FirstForm>
        </AppContext.Provider>
      )}
    </div>
  );
};

export default PlaylistGeneratorForm;
