import React, { useState } from "react";
import axios from "axios";
import Autocomplete from "@material-ui/lab/Autocomplete";
import InputAdornment from "@material-ui/core/InputAdornment";
import LocationOnOutlinedIcon from "@material-ui/icons/LocationOnOutlined";
import { TextField } from "@material-ui/core";
import "../../css/AddressField.css";
import { API_KEY } from "../../constants";

const AddressField = (props) => {
  const [address, setAddress] = useState("");
  const [addresses, setAddresses] = useState([]);
  const [where, setWhere] = useState({ lat: null, lng: null });
  const [error, setError] = useState(null);

  const handleChange = async (e) => {
    setAddress(e.target.value);
    props.onSelectAddress(e.target.value);
    let autosuggestUrl = "";

    if (!error) {
      autosuggestUrl = `/REST/v1/Autosuggest?query=${e.target.value}&userLocation=${where.lat},${where.lng},500&key=${API_KEY}`;
    } else {
      autosuggestUrl = `/REST/v1/Autosuggest?query=${e.target.value}&key=${API_KEY}`;
    }

    axios.get(autosuggestUrl).then((res) => {
      const suggestions = [];
      res.data.resourceSets[0].resources[0].value.map((suggestion) =>
        suggestions.push(suggestion.address.formattedAddress)
      );
      setAddresses(suggestions);
    });
  };

  const geoSuccess = (position) => {
    setWhere({
      lat: position.coords.latitude,
      lng: position.coords.longitude,
    });
  };

  const geoFailure = (err) => {
    setError(err.message);
  };

  const handleSelect = async () => {
    let geoOptions = {
      enableHighAccuracy: true,
      timeOut: 20000,
      maximumAge: 60 * 60,
    };

    navigator.geolocation.getCurrentPosition(
      geoSuccess,
      geoFailure,
      geoOptions
    );
  };

  return (
    <div style={{marginTop: "1%"}}>
      <Autocomplete
        freeSolo
        defaultValue={props.addressValue}
        onChange={(event, value) => {
          setAddress(value);
          props.onSelectAddress(value);
        }}
        disableClearable
        options={[...new Set(addresses)].map((suggestion) => suggestion)}
        renderInput={(params) => (
          <TextField
            style={{ width: "80%" }}
            value={address}
            onChange={handleChange}
            onClick={handleSelect}
            {...params}
            label={props.fieldLabel}
            error={props.errorMessage.length !== 0}
            margin="normal"
            variant="filled"
            color="secondary"
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <InputAdornment>
                  <LocationOnOutlinedIcon
                    style={{
                      color: "rgb(180, 180, 180)",
                      marginRight: "20px",
                      marginBottom: "20px",
                    }}
                  />
                </InputAdornment>
              ),
            }}
          />
        )}
      />
    </div>
  );
};

export default AddressField;
