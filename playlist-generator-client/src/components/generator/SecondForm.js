import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { AppContext } from "./PlaylistGeneratorForm";
import * as reducer from "../../store/reducers/generatorReducer";
import { API_KEY, PROGRESS_BAR_MAX_VAL } from "../../constants";
import {
  TextField,
  Input,
  InputAdornment,
  FormControlLabel,
  Checkbox,
  Typography,
  Tooltip,
  Zoom,
  Button,
} from "@material-ui/core";
import styled from "@emotion/styled/macro";
import axios from "axios";
import cookie from "js-cookie";
import Swal from "sweetalert2";

const SecondForm = () => {
  const history = useHistory();
  const userId = useSelector((state) => state.auth.user.id);
  const { state, dispatch } = useContext(AppContext);
  const [serverErrors, setServerErrors] = useState([]);

  const handlePrevious = () => {
    dispatch({ type: reducer.SWITCH_PREVIOUS_FORM });
  };

  const handlePercentageChange = (value, index) => {
    dispatch({
      type: reducer.HANDLE_PERCENTAGE_INPUT,
      value: value,
      index: index,
    });
  };

  const generatePlaylistCriteria = async () => {
    return await axios
      .get(
        `/REST/v1/Routes/DistanceMatrix?origins=${state.originCoords.lat},${state.originCoords.lng}&destinations=${state.destinationCoords.lat},${state.destinationCoords.lng}&travelMode=driving&key=${API_KEY}`
      )
      .then((result) => {
        const travelDuration =
          result.data.resourceSets[0].resources[0].results[0].travelDuration;
        return parseFloat(travelDuration.toFixed(2));
      })
      .then((duration) => {
        let preferences = [];
        for (const [k, v] of state.checkedGenres) {
          preferences.push({
            genreId: k.id,
            percentage: parseInt(v),
          });
        }

        const playlistRequestCriteria = {
          creatorId: userId,
          playlistTitle: state.playlistTitle,
          travelDuration: duration,
          playlistImage: state.image,
          topTracks: state.topTracks,
          sameArtist: state.sameArtist,
          jsonGenrePreferences: JSON.stringify(preferences),
        };

        return playlistRequestCriteria;
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const createFormData = (object) => {
    const formData = new FormData();
    Object.keys(object).forEach((key) => formData.append(key, object[key]));
    return formData;
  };

  const handlePlaylistGenerator = async () => {
    const playlistRequestCriteria = await generatePlaylistCriteria();

    const token = cookie.get("token");
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    await axios
      .post(`/api/playlist/generate`, createFormData(playlistRequestCriteria))
      .then((r) => {
        setServerErrors([]);
        Swal.fire({
          title: "Playlist Generated",
          type: "success",
          timer: "1300",
        });
        history.push("/my-profile");
      })
      .catch((e) => {
        setServerErrors(e.response.data.errors);
      });
  };

  const handleInputTitleChange = (event) => {
    dispatch({
      type: reducer.HANDLE_PLAYLIST_TITLE,
      value: event.target.value,
    });
  };

  const handleFileChange = (event) => {
    dispatch({
      type: reducer.CHANGE_PLAYLIST_COVER,
      file: event.target.files[0],
    });
  };

  const handleTopTracksChange = () => {
    dispatch({
      type: reducer.HANDLE_TOP_TRACKS,
    });
  };

  const handleSameArtistChange = () => {
    dispatch({
      type: reducer.HANDLE_SAME_ARTIST,
    });
  };

  const renderSelectedCheckboxes = () => {
    return [...state.checkedGenres.keys()].map((genre, index) => (
      <div key={genre.id}>
        <label id="genreLabel">{genre.name}</label>
        <Input
          id="percentInput"
          color="secondary"
          inputProps={{
            maxLength: 2,
          }}
          placeholder={
            state.checkedGenres.size === 1 ? `${PROGRESS_BAR_MAX_VAL}` : "0"
          }
          disabled={state.checkedGenres.size === 1}
          onChange={(event) =>
            handlePercentageChange(event.target.value, index)
          }
          endAdornment={
            <InputAdornment className="percentIcon" position="end">
              %
            </InputAdornment>
          }
        />
      </div>
    ));
  };

  const renderSecondForm = () => {
    return (
      <form className="box">
        <TextField
          label="Playlist Title"
          variant="filled"
          color="secondary"
          style={{ width: "75%" }}
          onChange={handleInputTitleChange}
        ></TextField>
        <div
          style={{
            margin: "3% 0 -1% 0",
            placeContent: "center",
            display: "flex",
          }}
        >
          <FormControlLabel
            style={{ marginLeft: 0, marginRight: 0 }}
            control={<Checkbox color="secondary" />}
            label={
              <Typography
                style={{
                  color: "rgb(210, 210, 210)",
                  letterSpacing: 1,
                  fontSize: 15,
                }}
              >
                Allow Same Artists
              </Typography>
            }
            labelPlacement="end"
            onChange={handleSameArtistChange}
          />
          <div style={{ width: "30%" }}></div>
          <FormControlLabel
            style={{ marginLeft: 0, marginRight: 0 }}
            control={<Checkbox color="secondary" />}
            label={
              <Typography
                style={{
                  color: "rgb(210, 210, 210)",
                  letterSpacing: 1,
                  fontSize: 15,
                }}
              >
                Use Top Tracks
              </Typography>
            }
            labelPlacement="start"
            onChange={handleTopTracksChange}
          />
        </div>
        {renderSelectedCheckboxes()}
        <progress
          max={PROGRESS_BAR_MAX_VAL}
          value={state.progressValue}
          data-label={state.progressValue + " %"}
        ></progress>
        <div style={{ display: "inline" }}>
          <Button
            id="generatorFormButtons"
            variant="contained"
            color="secondary"
            component="label"
          >
            Playlist Cover
            <Input
              type="file"
              inputProps={{ accept: "image/*" }}
              style={{ display: "none" }}
              onChange={handleFileChange}
            />
          </Button>
        </div>
        <div
          style={{ width: "16%", marginTop: "5%", display: "inline-block" }}
        ></div>
        <img id="playlistImg" alt="" src={state.imagePreview} />
        <div>
          <Button
            id="generatorFormButtons"
            variant="contained"
            color="secondary"
            onClick={handlePrevious}
          >
            Previous
          </Button>
          <div
            style={{ width: "16%", marginTop: "8%", display: "inline-block" }}
          ></div>
          <Tooltip
            TransitionComponent={Zoom}
            arrow
            placement="top"
            TransitionProps={{ timeout: 200 }}
            title={
              state.buttonGenerateDisabled
                ? "Playlist Title Playlist Cover and 100% Bar are required"
                : ""
            }
          >
            <span>
              <Button
                id="generatorFormButtons"
                disabled={state.buttonGenerateDisabled}
                variant="contained"
                color="secondary"
                onClick={handlePlaylistGenerator}
              >
                Generate Playlist
              </Button>
            </span>
          </Tooltip>
        </div>
        {serverErrors &&
          serverErrors.map((error, index) => (
            <ServerError key={index}>{error}</ServerError>
          ))}
      </form>
    );
  };

  return <div>{renderSecondForm()}</div>;
};

const ServerError = styled.p({
  color: "red",
  marginBottom: 18,
  fontWeight: 500,
});

export default SecondForm;
