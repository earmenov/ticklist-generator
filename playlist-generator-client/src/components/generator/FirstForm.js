import React, { useEffect, useContext } from "react";
import { AppContext } from "./PlaylistGeneratorForm";
import * as reducer from "../../store/reducers/generatorReducer";
import {
  FormGroup,
  Checkbox,
  FormControlLabel,
  Tooltip,
  Zoom,
  Button,
} from "@material-ui/core";
import { API_KEY } from "../../constants";
import axios from "axios";
import AddressField from "./AddressField";
import cookie from "js-cookie";

const FirstForm = () => {
  const { state, dispatch } = useContext(AppContext);
  const token = cookie.get("token");

  useEffect(() => {
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    axios
      .get(`/api/genres/get-all`)
      .then((fetchedGenres) => {
        dispatch({
          type: reducer.INIT_GENRES,
          fetchedGenres: fetchedGenres.data,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }, [dispatch, token]);

  const handleCheckboxChange = (genre) => (event) => {
    dispatch({ type: reducer.HANDLE_GENRE_CHANGE, genre: genre });
  };

  const handleGenreDisable = (genre) => {
    if (state.checkedGenres.size === 5) {
      return !state.checkedGenres.has(genre);
    }

    return false;
  };

  const handleNext = async () => {
    const addressErrors = {
      originError: "",
      destinationError: "",
    };
    addressErrors.originError = await validateAddressLocation(
      state.originAddress,
      true
    );
    addressErrors.destinationError = await validateAddressLocation(
      state.destinationAddress,
      false
    );
    dispatch({
      type: reducer.VALIDATE_ADDRESSES,
      addressErrors: addressErrors,
    });
    dispatch({ type: reducer.SWITCH_NEXT_FORM });
  };

  const handleOriginChange = (value) => {
    dispatch({ type: reducer.HANDLE_ORIGIN_ADDRESS, value: value });
  };

  const handleDestinationChange = (value) => {
    dispatch({ type: reducer.HANDLE_DESTINATION_ADDRESS, value: value });
  };

  const validateAddressLocation = async (location, isAddressOrigin) => {
    return await axios
      .get(`/REST/v1/Locations/${location}?key=${API_KEY}`)
      .then((result) => {
        return result.data.resourceSets[0].resources[0].point;
      })
      .then((result) => {
        const coords = {
          lat: result.coordinates[0],
          lng: result.coordinates[1],
        };
        isAddressOrigin
          ? dispatch({
              type: reducer.SET_COORDS,
              coords: coords,
              isAddressOrigin: true,
            })
          : dispatch({
              type: reducer.SET_COORDS,
              coords: coords,
              isAddressOrigin: false,
            });

        return "";
      })
      .catch(() => {
        return isAddressOrigin
          ? "Origin address invalid. Please try another"
          : "Destination address invalid. Please try another";
      });
  };

  const renderCheckboxes = () => {
    return state.genres.map((genre) => (
      <FormControlLabel
        key={genre.id}
        control={
          <Checkbox
            disabled={handleGenreDisable(genre)}
            onChange={handleCheckboxChange(genre)}
            color="secondary"
          />
        }
        label={genre.name}
        labelPlacement="end"
      />
    ));
  };

  const renderFirstForm = () => {
    return (
      <form className="box">
        <AddressField
          fieldLabel={"Origin Address"}
          errorMessage={state.errors.originAddressError}
          onSelectAddress={handleOriginChange}
        />
        {state.errors.originAddressError.length !== 0 && (
          <p id="errorMessage">{state.errors.originAddressError}</p>
        )}
        <AddressField
          fieldLabel={"Destination Address"}
          errorMessage={state.errors.destinationAddressError}
          onSelectAddress={handleDestinationChange}
        />
        {state.errors.destinationAddressError.length !== 0 && (
          <p id="errorMessage">{state.errors.destinationAddressError}</p>
        )}
        <FormGroup id="checkboxGroup" row>
          {state.genres && renderCheckboxes()}
        </FormGroup>
        <Tooltip
          TransitionComponent={Zoom}
          arrow
          placement="top"
          TransitionProps={{ timeout: 200 }}
          title={
            state.buttonNextDisabled
              ? "Fill address fields and select playlist genres"
              : ""
          }
        >
          <span>
            <Button
              id="generatorFormButtons"
              variant="contained"
              disabled={state.buttonNextDisabled}
              color="secondary"
              onClick={handleNext}
            >
              Next
            </Button>
          </span>
        </Tooltip>
      </form>
    );
  };
  return <div>{renderFirstForm()}</div>;
};

export default FirstForm;
