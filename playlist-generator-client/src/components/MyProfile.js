import React from "react";
import { useSelector } from "react-redux";
import PlaylistDisplayer from "./PlaylistDisplayer";
import Navbar from "./Navbar";
import UserCard from "./UserCard";
import Divider from "@material-ui/core/Divider";
import styled from "@emotion/styled/macro";

const MyProfile = () => {
  const currUser = useSelector((state) => state.auth.user);

  return (
    <div>
      <Navbar></Navbar>
      <UserCard user={currUser}></UserCard>
      <Header>PLAYLIST OVERVIEW</Header>
      <Divider style={{ height: 2, margin: "15% 9% 5% 9%" }}></Divider>
      <PlaylistDisplayer
        userId={currUser.id}
        isEditable={true}
      ></PlaylistDisplayer>
    </div>
  );
};

const Header = styled.h2({
  fontVariant: "petite-caps",
  color: "#38393b",
  fontSize: "33px",
  letterSpacing: 1,
  margin: "12.5% 0 0 5%",
  position: "absolute",
});

export default MyProfile;
