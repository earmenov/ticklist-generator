import React, { useEffect, useState } from "react";
import axios from "axios";
import PlaylistDisplayer from "./PlaylistDisplayer";
import Navbar from "./Navbar";
import UserCard from "./UserCard";
import Divider from "@material-ui/core/Divider";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import styled from "@emotion/styled/macro";

const UserProfile = (props) => {
  const [user, setUser] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const userId = window.location.href.split("/").pop();
    axios
      .get(`/api/user/${userId}`)
      .then((res) => {
        setUser(res.data);
        setIsLoading(false);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  return (
    <div>
      <Navbar></Navbar>
      {isLoading ? (
        <Backdrop open={isLoading}>
          <CircularProgress color="secondary" />
        </Backdrop>
      ) : (
        <div>
          <UserCard user={user}></UserCard>
          <Header>PLAYLIST OVERVIEW</Header>
          <Divider style={{ height: 2, margin: "15% 9% 5% 9%" }}></Divider>
          <PlaylistDisplayer userId={user.id}></PlaylistDisplayer>
        </div>
      )}
    </div>
  );
};

const Header = styled.h2({
  color: "#38393b",
  letterSpacing: 1,
  fontSize: "33px",
  margin: "12.5% 0 0 5%",
  position: "absolute",
});

export default UserProfile;
