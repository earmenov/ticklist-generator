import React, { useState, useEffect } from "react";
import axios from "axios";
import "../css/UserCard.css";

const UserCard = (props) => {
  const [playlistCount, setPlaylistCount] = useState("-");

  useEffect(() => {
    axios
      .get(`/api/playlist/count-for-user/${props.user.id}`)
      .then((res) => {
        setPlaylistCount(res.data.toString());
      })
      .catch((e) => {
        console.log(e);
      });
  }, [props.user.id]);

  return (
    <div className="card">
      <div className="avatar"></div>
      <div className="cover"></div>
      <div className="userinfomain">
        <h1 className="infoName">{props.user.username}</h1>
        <h2 className="infoMail">{props.user.email}</h2>
      </div>
      <div className="divider"></div>
      <div className="socialinfo">
        <div className="socialone">
          <span className="socialtext">42</span>
          <br />
          <span className="socialheading">Followers</span>
        </div>
        <div className="socialtwo">
          <span className="socialtext">{playlistCount}</span>
          <br />
          <span className="socialheading">Playlists</span>
        </div>
        <div className="socialthree">
          <span className="socialtext">7</span>
          <br />
          <span className="socialheading">Following</span>
        </div>
      </div>
    </div>
  );
};

export default UserCard;
