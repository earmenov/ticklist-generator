import React, { useState, useRef, useCallback } from "react";
import PlaylistCard from "./PlaylistCard";
import Grid from "@material-ui/core/Grid";
import usePlaylistGuestSearch from "../hooks/usePlaylistGuestSearch";
import RootRef from "@material-ui/core/RootRef";
import CircularProgress from "@material-ui/core/CircularProgress";
import FilterPlaylist from "./FilterPlaylist";
import styled from "@emotion/styled/macro";
import Link from "@material-ui/core/Link";

const PlaylistDisplayer = (props) => {
  const stringSimilarity = require("string-similarity");
  const [pageNumber, setPageNumber] = useState(0);
  const [searchCriteria, setSearchCriteria] = useState({
    playlistTitle: "",
    fromDuration: 0,
    toDuration: 24,
    checkedGenres: [],
    sortBy: "",
    asc: false,
  });
  const { playlists, hasMore, loading } = usePlaylistGuestSearch(
    props.userId
      ? `/api/playlist/get-all/profile/${props.userId}/?title=${searchCriteria.playlistTitle}&fromDuration=${searchCriteria.fromDuration}&toDuration=${searchCriteria.toDuration}&checkedGenres=${searchCriteria.checkedGenres}&sortBy=${searchCriteria.sortBy}&asc=${searchCriteria.asc}&page=${pageNumber}&size=8`
      : `/api/playlist/guest/filter?title=${searchCriteria.playlistTitle}&fromDuration=${searchCriteria.fromDuration}&toDuration=${searchCriteria.toDuration}&checkedGenres=${searchCriteria.checkedGenres}&sortBy=${searchCriteria.sortBy}&asc=${searchCriteria.asc}&page=${pageNumber}&size=8`, pageNumber
  );

  const getSearchCriteria = (criteria) => {
    setPageNumber(0);
    setSearchCriteria(criteria);
  };

  const observer = useRef();
  const lastPlaylistRef = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver(
        (entries) => {
          if (entries[0].isIntersecting && hasMore) {
            setPageNumber((prevPageNumber) => prevPageNumber + 1);
          }
        },
        { threshold: 1 }
      );
      if (node) observer.current.observe(node);
    },
    [hasMore, loading]
  );

  const showPlaylistCard = (playlist) => {
    return (
      <Grid key={playlist.id} item>
        <PlaylistCard
          playlist={playlist}
          isEditable={props.isEditable}
        ></PlaylistCard>
        {props.showUser && (
          <Username>
            <Link
              color="inherit"
              href={`/user/show/${playlist.user.id}`}
              target="_blank"
            >
              {playlist.user.username}
            </Link>
          </Username>
        )}
      </Grid>
    );
  };

  const displayPlaylists = () => {
    return (
      <Grid
        item
        xs={12}
        container
        justify="center"
        spacing={10}
        style={{ margin: 0 }}
      >
        {playlists
          .filter((playlist) =>
            props.username && props.username.length > 0
              ? stringSimilarity.compareTwoStrings(
                  playlist.user.username,
                  props.username
                ) > 0.6
              : true
          )
          .map((playlist, index) => {
            if (playlists.length === index + 1) {
              return (
                <RootRef key={playlist.id} rootRef={lastPlaylistRef}>
                  {showPlaylistCard(playlist)}
                </RootRef>
              );
            } else {
              return showPlaylistCard(playlist);
            }
          })}
        {loading && (
          <Grid key={playlists.length + 1} item>
            <CircularProgress style={{ marginTop: 150 }}></CircularProgress>
          </Grid>
        )}
      </Grid>
    );
  };

  return (
    <div>
      {playlists.length > 1 && (
        <FilterPlaylist onSearchSelect={getSearchCriteria}></FilterPlaylist>
      )}
      {playlists.length > 0 && displayPlaylists()}
    </div>
  );
};

const Username = styled.h3({
  textAlign: "center",
  color: "#444444",
  marginTop: "4%",
  fontSize: 18,
  letterSpacing: 1,
});

export default PlaylistDisplayer;
