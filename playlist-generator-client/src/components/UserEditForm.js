import React, { useState } from "react";
import axios from "axios";
import store from "../store/index";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Link from "@material-ui/core/Link";
import Swal from "sweetalert2";
import cookie from "js-cookie";
import { VALID_EMAIL_PATTERN } from "../constants";
import { useDispatch } from "react-redux";
import Divider from "@material-ui/core/Divider";
import styled from "@emotion/styled/macro";

const UserEditForm = React.forwardRef((props, ref) => {
  const classes = useStyles();
  ref = { ref };

  const [changePassword, setChangePassword] = useState(false);
  const [serverErrors, setServerErrors] = useState([]);
  const [editErrors, setEditErrors] = useState({});
  const [editUser, setEditUser] = useState({
    username: store.getState().auth.user.username,
    email: store.getState().auth.user.email,
    currPassword: "",
    newPassword: "",
    confPassword: "",
  });
  const dispatch = useDispatch();

  const validateEditForm = (values) => {
    let errors = {};
    const validEmailRegex = RegExp(VALID_EMAIL_PATTERN);

    if (!values.username) {
      errors.username = "This field is required";
    } else if (values.username.length < 5) {
      errors.username = "Username needs to be more than 5 characters";
    }

    if (!values.email) {
      errors.email = "This field is required";
    } else if (!validEmailRegex.test(values.email)) {
      errors.email = "Email address is invalid";
    }

    if (changePassword) {
      if (!values.currPassword) {
        errors.currPassword = "This field is required";
      } else if (values.currPassword.length < 8) {
        errors.currPassword = "Password needs to be more than 8 characters";
      }

      if (!values.newPassword) {
        errors.newPassword = "This field is required";
      } else if (values.newPassword.length < 8) {
        errors.newPassword = "Password needs to be more than 8 characters";
      }

      if (!values.confPassword) {
        errors.confPassword = "This field is required";
      } else if (values.confPassword !== values.newPassword) {
        errors.confPassword = "New passwords do not match";
      }
    }
    return errors;
  };

  const handleSuccessfulEdit = () => {
    setServerErrors([]);
    Swal.fire({
      title: "User Edit Successful",
      type: "success",
      timer: "1500",
    });

    props.closeModal();
    setEditUser({
      ...editUser,
      currPassword: "",
      newPassword: "",
      confPassword: "",
    });
  };

  const submitEditUser = () => {
    const errors = validateEditForm(editUser);
    setEditErrors(errors);

    if (Object.keys(errors).length === 0) {
      const editRequest = {
        id: store.getState().auth.user.id,
        ...editUser,
      };

      const token = cookie.get("token");

      axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
      axios
        .put(`/api/users/update`, editRequest)
        .then(() => {
          axios.defaults.headers.common["Authorization"] = `Bearer ${cookie.get(
            "token"
          )}`;
          axios
            .get(`/api/user/me`)
            .then((res) => {
              dispatch({ type: "SET_LOGIN", payload: res.data });
            })
            .catch(() => {
              cookie.remove("token");
              window.location.reload();
            });
        })
        .then(() => {
          handleSuccessfulEdit();
        })
        .catch((e) => {
          setServerErrors(e.response.data.errors);
        });
    }
  };

  const cancelPasswordChange = () => {
    setChangePassword(false);
    setEditUser({
      ...editUser,
      currPassword: "",
      newPassword: "",
      confPassword: "",
    });
  };

  const handleEditChange = (event) => {
    const { name, value } = event.target;
    setEditUser({
      ...editUser,
      [name]: value,
    });
  };

  const showPasswordFields = () => {
    return (
      <div>
        <TextField
          label="Current Password"
          variant="outlined"
          name="currPassword"
          error={editErrors.currPassword ? true : false}
          autoComplete="new-password"
          type="password"
          helperText={editErrors.currPassword && editErrors.currPassword}
          className={classes.userEditInput}
          onChange={handleEditChange}
        />
        <TextField
          label="New Password"
          variant="outlined"
          name="newPassword"
          error={editErrors.newPassword ? true : false}
          type="password"
          helperText={editErrors.newPassword && editErrors.newPassword}
          className={classes.userEditInput}
          onChange={handleEditChange}
        />
        <TextField
          label="Confirm Password"
          variant="outlined"
          name="confPassword"
          error={editErrors.confPassword ? true : false}
          type="password"
          helperText={editErrors.confPassword && editErrors.confPassword}
          className={classes.userEditInput}
          onChange={handleEditChange}
        />
        <div>
          <Link href="#" onClick={cancelPasswordChange}>
            cancel password change
          </Link>
        </div>
      </div>
    );
  };

  return (
    <EditForm>
      <Header>Edit User</Header>
      <Divider style={{ marginBottom: 25, margin: "0 8% 4% 8%" }} />
      <TextField
        label="Username"
        variant="outlined"
        name="username"
        inputProps={{ maxLength: 20 }}
        error={editErrors.username ? true : false}
        defaultValue={editUser.username}
        helperText={editErrors.username && editErrors.username}
        className={classes.userEditInput}
        onChange={handleEditChange}
      />
      <TextField
        label="Email"
        variant="outlined"
        name="email"
        error={editErrors.email ? true : false}
        defaultValue={editUser.email}
        helperText={editErrors.email && editErrors.email}
        className={classes.userEditInput}
        onChange={handleEditChange}
      />
      {changePassword ? (
        showPasswordFields()
      ) : (
        <div>
          <Link
            href="#"
            onClick={() => {
              setChangePassword(true);
            }}
          >
            change password
          </Link>
        </div>
      )}
      <Button
        variant="contained"
        onClick={props.closeModal}
        className={classes.editFormButton}
      >
        Cancel
      </Button>
      <Button
        variant="contained"
        color="secondary"
        className={classes.editFormButton}
        onClick={submitEditUser}
      >
        Save
      </Button>
      <Divider style={{ marginBottom: 25, margin: "2% 8% 0 8%" }} />
      {serverErrors &&
        serverErrors.map((error, index) => (
          <ServerError key={index}>{error}</ServerError>
        ))}
    </EditForm>
  );
});

const EditForm = styled.form({
  position: "absolute",
  width: "45%",
  left: "26.7%",
  padding: "40px",
  background: "whitesmoke",
  boxShadow: "0 15px 25px rgba(0, 0, 0, 0.5)",
  borderRadius: "15px",
  textAlign: "center",
});

const Header = styled.h2({
  fontVariant: "petite-caps",
  letterSpacing: 1,
  margin: "0px 8px 20px 0px",
});

const ServerError = styled.p({
  color: "red",
  marginBottom: 18,
  fontWeight: 500,
});

const useStyles = makeStyles(() => ({
  userEditInput: {
    width: "70%",
    marginBottom: 25,
  },
  editFormButton: {
    margin: "3% 5% 1% 5%",
    width: "20%",
    letterSpacing: 1,
    fontSize: 13,
    padding: "9px 0 9px 0",
  },
}));

export default UserEditForm;
